# compile designer's UI file

IF_DIR := interfaces
MO_DIR := modules
TR_DIR := translation

NAMES := \
	projection \
	part_angles part_brightness part_contrast part_distance part_room part_screen part_sizes

INTERFACES := $(addprefix $(IF_DIR)/, $(addsuffix .ui,$(NAMES)))
MODULES := $(addprefix $(MO_DIR)/, $(addsuffix _ui.py,$(NAMES)))
SOURCES := $(addprefix $(MO_DIR)/, $(addsuffix .py,$(NAMES)))

all: $(MODULES) $(TR_DIR)/projection_ru.qm

$(MO_DIR)/%_ui.py: $(IF_DIR)/%.ui
	python3 -m PyQt5.uic.pyuic -o $@ $<

$(TR_DIR)/projection_ru.qm: $(TR_DIR)/modules_ru.ts $(TR_DIR)/interfaces_ru.ts
	lrelease -qt=qt5 $(TR_DIR)/modules_ru.ts $(TR_DIR)/interfaces_ru.ts -qm $(TR_DIR)/projection_ru.qm

$(TR_DIR)/interfaces_ru.ts: $(INTERFACES)
	lupdate -qt=qt5 -source-language en -target-language ru $(IF_DIR)/*.ui -ts $(TR_DIR)/interfaces_ru.ts -verbose

$(TR_DIR)/modules_ru.ts: $(SOURCES)
	pylupdate5 -verbose $(SOURCES) -ts $(TR_DIR)/modules_ru.ts

run: 
	python3 ./projection.py

clean:
	rm $(MO_DIR)/*.pyc
	cd $(MO_DIR) && rm __pycache__ -rf

#$(IF_DIR)/projection_ru.ts: $(INTERFACES)
#	lupdate -qt=qt5 -source-language en -target-language ru $(IF_DIR)/*.ui -ts $@ -verbose
