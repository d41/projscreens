# ProjScreens

## About

Collection of calculators:

* screen size by room size;
* screen sizes by one side in different aspect ratio;
* brightness correction for cases when aspect ratios doesn't match;
* angular screen sizes;
* other minor details.

## Requirements

* Python3 (tested with 3.6.9)
* PyQt5 (tested with 5.10)

## Details

Started as weekend project to try PyQt5.

Currently screen size decided by nearest viewer and farthest viewer by simple method.  
Maybe later I add method similar to InfoComm, as described in Samsung's [Guide to Choosing the Right Digital Signage Size](https://pid.samsungdisplay.com/en/learning-center/blog/how-to-choose-signage-size).

No logging, no schemes, primitive export to file. Already usefull for me.
 
```
$ ./projection.py
```

## Current state
1. Recovered from Mercurial repository
2. Half translated (in TODO list)
3. Refactoring of internals and UI planned. Because I want to add some schemes and pictures.


## Calculators with schemes
Still in debug mode.

### Room Screen

Same things that were in Room and Screen calculators. But now with simple schemes.

## Cylindrical Screen

Basic calculator to check projector manual tables about possibilities of geometry correction.

Limitations:
* No input filtering

### Projection Blending

Calculate required projectors amount to completely fill projection surface.
Try to find optimal variant based on projectors aspect ratio.

Limitations:
* Rectangular horizontal projection surface
* All projectors have same characteristics
* Projector fill complete height (no vertical masking)
* Actual overlap calculation based on desired minimum (can be bigger, but can't be smaller)
* Variants of different aspect ratio projector shown sorted (based on optimal blend percentage)
* Draw sample installation schemes
* Save to file (via print preview dialog with save to pdf option)

