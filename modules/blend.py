#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import math
import os.path
from collections import namedtuple

from PyQt5 import QtWidgets, QtCore, QtGui

from modules import blend_ui
from modules import calculate
from modules.schemes import Scheme

logger = logging.getLogger(__name__)

ProjectionInfo = namedtuple("ProjectionInfo", ("width", "height", "res_x", "res_y"))
MultiBlock = namedtuple("MultiBlock", ("side_step", "main_step", "count", "distance"))


class ProjectorBlend(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.ui = blend_ui.Ui_ProjectorBlend()
        self.ui.setupUi(self)

        self.setup_headers()

        self.frontScheme = FrontScheme(self.ui.gv_front)
        self.ui.gv_front.setScene(self.frontScheme)

        self.topScheme = TopScheme(self.ui.gv_top)
        self.ui.gv_top.setScene(self.topScheme)

        self.data_for_export = None

        self.ui.variants.itemSelectionChanged.connect(self.set_resolution_list)

    def setup_headers(self):
        """
        Fill header names in table.
        """
        header = self.ui.variants.horizontalHeader()
        # Set headers
        self.ui.variants.setHorizontalHeaderLabels([
            header.tr("Aspect"), header.tr("Blend, %"), header.tr("Blend, mm"), header.tr("Projectors")
        ])

    @QtCore.pyqtSlot()
    def on_calculate_blend_clicked(self):
        """
        Click on Calculate button processed here.

        Two steps:
        1. Calculate projectors for required minimal blend percentage (three aspect ratios).
        2. Calculate percentage for actual amount of projectors (three aspect ratios).

        And then fill the table sorted by percentage.
        """

        # clear table, to be able to check that calculation started, but failed
        self.ui.variants.clearContents()

        width = self.ui.surfaceWidth.value()
        height = self.ui.surfaceHeight.value()
        blend = self.ui.blendMinimal.value()
        logger.info("data: w={}, h={}, b={}".format(width, height, blend))
        self.clear_forms_data()

        if width < height:
            logger.error("not supported layout in this version")
            return

        # check for real blend for every aspect 4:3, 16:10, 16:9 and count projectors
        variants = []
        for aspect in [12, 10, 9]:
            variants.append(calculate.get_real_blend(width, height, aspect, blend))

        variants.sort(key=lambda x: x[1])

        for num, var in enumerate(variants):
            # Round some values, simplify aspect
            test = math.gcd(16, var[0])
            if test == 4:
                text_aspect = "{:d}:{:d}".format(int(16 / test), int(var[0] / test))
            else:
                text_aspect = "16:{}".format(var[0])
            cell_aspect = QtWidgets.QTableWidgetItem(text_aspect)
            cell_aspect.setData(QtCore.Qt.UserRole, var[0])

            cell_bpercent = QtWidgets.QTableWidgetItem("{:.01f}".format(var[1]))
            cell_bpercent.setData(QtCore.Qt.UserRole, var[1])

            cell_bwidth = QtWidgets.QTableWidgetItem("{:.01f}".format(var[2]))

            cell_proj_count = QtWidgets.QTableWidgetItem("{}".format(var[3]))
            cell_proj_count.setData(QtCore.Qt.UserRole, var[3])

            self.ui.variants.setItem(num, 0, cell_aspect)
            self.ui.variants.setItem(num, 1, cell_bpercent)
            self.ui.variants.setItem(num, 2, cell_bwidth)
            self.ui.variants.setItem(num, 3, cell_proj_count)

    def set_resolution_list(self):
        """
        Fill combo box with resolutions matching selected aspect ratio
        """
        resolutions = {
            9: [(1920, 1080), (1280, 720)],
            10: [(1920, 1200), (1280, 800)],
            12: [(1400, 1050), (1024, 768)],
        }

        self.ui.resolutions.clear()
        items = self.ui.variants.selectedItems()

        found_items = len(items) != 0
        if found_items:
            aspect = items[0].data(QtCore.Qt.UserRole)
            if aspect in resolutions:
                for x, y in resolutions[aspect]:
                    self.ui.resolutions.addItem("{} x {}".format(x, y), userData=(x, y))
            else:
                logger.error("Wrong selected aspect ratio!")

        self.ui.resolutions.setEnabled(found_items)
        self.ui.draw_schemes.setEnabled(found_items)
        self.clear_forms_data()

    @QtCore.pyqtSlot()
    def on_draw_schemes_clicked(self):
        """
        Calculate surface size and edge blending in pixels, and projectors locations
        """

        # get main info
        surface_w, surface_h = self.ui.surfaceWidth.value(), self.ui.surfaceHeight.value()
        projector_x, projector_y = self.ui.resolutions.currentData(QtCore.Qt.UserRole)

        # gathering additional info
        items = self.ui.variants.selectedItems()
        found_items = len(items) != 0
        if found_items:
            aspect = items[0].data(QtCore.Qt.UserRole)
            blend = items[1].data(QtCore.Qt.UserRole)
            projector_count = items[3].data(QtCore.Qt.UserRole)
        throw_ratio = self.ui.throwRatio.value()

        # gather single projection info
        projector_w, projector_h = (surface_h / aspect) * 16, surface_h
        projection = ProjectionInfo(projector_w, projector_h, projector_x, projector_y)

        # gather surface info
        blend_x = projector_x * (blend / 100.0)

        if projector_count > 1:
            surface_x = int((projector_x - blend_x) * projector_count + blend_x)
        else:
            surface_x = int(projector_y / surface_h * surface_w)

        surface_y = projector_y
        surface = ProjectionInfo(surface_w, surface_h, surface_x, surface_y)
        self.ui.print_data.setEnabled(True)
        self.ui.copy_data.setEnabled(True)

        # additional info
        blend_w = projector_w * (blend / 100.0)

        multi_projectors = MultiBlock(
            side_step=projector_w / 2,
            main_step=projector_w - blend_w,
            count=projector_count,
            distance=projector_w * throw_ratio,
        )

        # draw schemes
        self.frontScheme.initializeItems(surface, multi_projectors, projection, scale=0.05)
        self.topScheme.initializeItems(surface, multi_projectors, scale=0.05)

        # fill form values
        self.ui.surfaceResolution.setText("{} x {}".format(surface.res_x, surface.res_y))
        self.ui.projectionSize.setText("{:0.0f} x {:0.0f}".format(projector_w, projector_h))
        self.ui.pixelSize.setText("{:0.1f}".format(surface_h / surface_y))

        self.data_for_export = {
            "surface": surface,
            "projection": projection,
            "blend": (blend, blend_w, blend_x),
            "aspect": aspect,
            "multi_projectors": multi_projectors,
            "resolutions": self.ui.resolutions.currentText(),
        }

    def clear_forms_data(self):
        self.ui.surfaceResolution.setText("")
        self.ui.projectionSize.setText("")
        self.ui.pixelSize.setText("")
        self.ui.print_data.setEnabled(False)
        self.ui.copy_data.setEnabled(False)

    @QtCore.pyqtSlot(int)
    def on_scaleFactor_valueChanged(self, value):
        """
        Scale schematics of projection surface

        :param value: scale factor, 5 = 0,5x, 10 = 1x, 20=2x
        :return:
        """
        self.scale = value / 10.0
        self.ui.lScale.setText("{:0.1f}x".format(self.scale))

        self.ui.gv_front.resetTransform()
        self.ui.gv_top.resetTransform()
        self.ui.gv_front.scale(self.scale, self.scale)
        self.ui.gv_top.scale(self.scale, self.scale)

    def prepare_text(self):
        # prepare and process actual data
        export_data = {
            "surface_id": self.ui.surfaceID.text(),
            "notes": self.ui.notes.toPlainText().replace("\n", "<br/>"),
            "surface_size": "{} x {}".format(self.ui.surfaceWidth.value(), self.ui.surfaceHeight.value()),
            "surface_resolution": self.ui.surfaceResolution.text(),
            "projector_size": self.ui.projectionSize.text(),
            "throw_ratio": self.ui.throwRatio.text(),
            "pixel_size": self.ui.pixelSize.text().replace('.', ','),

            "projectors_count": "{}".format(self.data_for_export["multi_projectors"].count),
            "blend_mm": "{:0.0f}".format(self.data_for_export["blend"][1]),
            "blend_px": "{:0.0f}".format(self.data_for_export["blend"][2]),
            "projector_resolution": self.data_for_export["resolutions"],
            "distance": "{:0.0f}".format(self.data_for_export["multi_projectors"].distance),
            "side_step": "{:0.0f}".format(self.data_for_export["multi_projectors"].side_step),
            "main_step": "{:0.0f}".format(self.data_for_export["multi_projectors"].main_step),
        }
        logger.debug(export_data)

        text = ""
        with open(os.path.join("templates", "blend.html"), "r") as f:
            template = f.read()
            text = template.format(**export_data)

        return text

    def get_document(self):
        editor = QtWidgets.QTextEdit()
        document = QtGui.QTextDocument()
        editor.setDocument(document)

        front_image = self.ui.gv_front.scene().get_image()
        document.addResource(
            QtGui.QTextDocument.ImageResource,
            QtCore.QUrl("dynamic:/images/blend_front.png"),
            front_image)

        top_image = self.ui.gv_top.scene().get_image()
        document.addResource(
            QtGui.QTextDocument.ImageResource,
            QtCore.QUrl("dynamic:/images/blend_top.png"),
            top_image)

        text = self.prepare_text()
        editor.append(text)
        document.end()

        return document

    @QtCore.pyqtSlot()
    def on_copy_data_clicked(self):
        print("Copy to clipboard:")
        document = self.get_document()

        data = QtCore.QMimeData()
        data.setHtml(document.toHtml())

        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setMimeData(data, QtGui.QClipboard.Clipboard)

    @QtCore.pyqtSlot()
    def on_print_data_clicked(self):
        """
        Export results to file. Right now it is printing. (Print to file option).

        :return: nothing
        """
        editor = QtWidgets.QTextEdit()
        document = QtGui.QTextDocument()
        editor.setDocument(document)

        scene_w, scene_h = self.ui.gv_front.scene().width(), self.ui.gv_front.scene().height()
        image = QtGui.QImage(scene_w, scene_h, QtGui.QImage.Format_ARGB32_Premultiplied)
        image.fill(0)
        painter = QtGui.QPainter(image)
        self.ui.gv_front.scene().render(painter)
        painter.end()

        # image.save("front.png")  # this is test
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/blend_front.png"), image)

        scene_w, scene_h = self.ui.gv_top.scene().width(), self.ui.gv_top.scene().height()
        image = QtGui.QImage(scene_w, scene_h, QtGui.QImage.Format_ARGB32_Premultiplied)
        image.fill(0)
        painter = QtGui.QPainter(image)
        self.ui.gv_top.scene().render(painter)
        painter.end()

        # image.save("top.png")  # this is test
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/blend_top.png"), image)

        # TODO: prepare export document (print preview just for visual test)
        # prepare and process actual data
        export_data = {
            "surface_id": self.ui.surfaceID.text(),
            "notes": self.ui.notes.toPlainText().replace("\n", "<br/>"),
            "surface_size": "{} x {}".format(self.ui.surfaceWidth.value(), self.ui.surfaceHeight.value()),
            "surface_resolution": self.ui.surfaceResolution.text(),
            "projector_size": self.ui.projectionSize.text(),
            "throw_ratio": self.ui.throwRatio.text(),
            "pixel_size": self.ui.pixelSize.text().replace('.', ','),

            "projectors_count": "{}".format(self.data_for_export["multi_projectors"].count),
            "blend_mm": "{:0.0f}".format(self.data_for_export["blend"][1]),
            "blend_px": "{:0.0f}".format(self.data_for_export["blend"][2]),
            "projector_resolution": self.data_for_export["resolutions"],
            "distance": "{:0.0f}".format(self.data_for_export["multi_projectors"].distance),
            "side_step": "{:0.0f}".format(self.data_for_export["multi_projectors"].side_step),
            "main_step": "{:0.0f}".format(self.data_for_export["multi_projectors"].main_step),
        }
        print(export_data)

        with open(os.path.join("templates", "blend.html"), "r") as f:
            template = f.read()
            text = template.format(**export_data)
            editor.append(text)
            document.end()

        from PyQt5 import QtPrintSupport
        printer = QtPrintSupport.QPrinter()
        print_dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        print_dialog.paintRequested.connect(editor.print)
        if print_dialog.exec() != print_dialog.Accepted:
            return

        document.print(printer)


projector = (400, 400, 200)  # width, depth, height


class FrontScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.surface = None
        self.projections = None
        self.projectors = None
        self.annotations = None

        self.resetItems()

    def resetItems(self):
        self.surface = None
        self.projections = []
        self.projectors = []
        self.annotations = []

    def initializeItems(self, surface, multi_proj, projection, scale=1.0):
        self.clear()
        self.resetItems()

        # prepare scaled values
        sw, sh = surface.width * scale, surface.height * scale
        pw, ph = projection.width * scale, projection.height * scale
        side = multi_proj.side_step * scale
        main = multi_proj.main_step * scale
        prw, prd, prh = projector[0] * scale, projector[1] * scale, projector[2] * scale

        # draw surface
        self.surface = self.addRect(0, 0, sw, sh, pen=self.pen_surface)
        d = self.drawHorizontalDimension(0, sh, sw, sh, -20, "{:.00f} : {} px".format(surface.width, surface.res_x))
        self.annotations.append(d)
        d = self.drawVerticalDimension(0, 0, 0, sh, -15, "{:.00f} : {} px".format(surface.height, surface.res_y))
        self.annotations.append(d)

        # draw projections
        for num in range(multi_proj.count):
            h_shift = num * main
            item = self.addRect(h_shift, 0, pw, ph, pen=self.pen_projection)
            self.projections.append(item)

            item = self.addRect(
                (side + h_shift) - prw / 2, -(10.0 + prh),
                prw, prh,
                pen=self.pen_projector)
            self.projectors.append(item)

            x, y = side + h_shift, -10
            item = self.addLine(x, y, h_shift, 0, pen=self.pen_illusion)
            self.annotations.append(item)
            item = self.addLine(x, y, h_shift, sh, pen=self.pen_illusion)
            self.annotations.append(item)
            item = self.addLine(x, y, x + side, 0, pen=self.pen_illusion)
            self.annotations.append(item)
            item = self.addLine(x, y, x + side, sh, pen=self.pen_illusion)
            self.annotations.append(item)

        d = self.drawHorizontalDimension(0, 0, pw, 0, 20, "{:.00f} : {} px".format(projection.width, projection.res_x))
        self.annotations.append(d)

        d = self.drawHorizontalDimension(main, 0, pw, 0, 40, "{:.00f}".format(projection.width - multi_proj.main_step))
        self.annotations.append(d)


class TopScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.surface = None
        self.projectors = None
        self.annotations = None

        self.resetItems()

    def resetItems(self):
        self.surface = None
        self.projectors = []
        self.annotations = []

    def initializeItems(self, surface, multi_proj, scale=1.0):
        self.clear()
        self.resetItems()

        # prepare scaled values
        sw, sh = surface.width * scale, surface.height * scale
        side = multi_proj.side_step * scale
        main = multi_proj.main_step * scale
        distance = multi_proj.distance * scale
        prw, prd, prh = projector[0] * scale, projector[1] * scale, projector[2] * scale

        # draw surface
        self.surface = self.addLine(0, 0, sw, 0, pen=self.pen_surface)
        d = self.drawHorizontalDimension(0, 0, sw, 0, -15, "{:.00f}".format(surface.width))
        self.annotations.append(d)
        d = self.drawVerticalDimension(0, 0, side, distance, -15, "{:.00f}".format(multi_proj.distance))
        self.annotations.append(d)

        # draw projections
        for num in range(multi_proj.count):
            h_shift = num * main
            item = self.addRect(
                (side + h_shift) - prw / 2, distance,
                prw, prd,
                pen=self.pen_projector)
            self.projectors.append(item)

            x, y = side + h_shift, distance
            item = self.addLine(x, y, h_shift, 0, pen=self.pen_illusion)
            self.annotations.append(item)
            item = self.addLine(x, y, x + side, 0, pen=self.pen_illusion)
            self.annotations.append(item)

        # draw steps
        d = self.drawHorizontalDimension(0, 0, side, distance, distance + 40, "{:.00f}".format(multi_proj.side_step))
        self.annotations.append(d)

        if multi_proj.count > 1:
            d = self.drawHorizontalDimension(
                side, distance,
                side + main, distance,
                distance + 40,
                "{:.00f}".format(multi_proj.main_step))
            self.annotations.append(d)


if '__main__'==__name__:
    import sys
    import blend_ui
    import calculate
    from schemes import Scheme

    app = QtWidgets.QApplication(sys.argv)
    win = ProjectorBlend()
    win.show()
    sys.exit(app.exec_())
