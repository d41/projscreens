#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import math

__author__ = "d41"

logger = logging.getLogger(__name__)


def throw_distance(throw_ratio, width):
    """ Проекционное расстояние по проекционному отношению и ширине экрана
    """
    return throw_ratio * width


def arc_angle(radius, chord_length):
    """ Поиск угла дуги окружности по радиусу и хорде
    """
    angle = math.asin((chord_length / 2) / radius)
    return angle * 2, math.degrees(angle * 2)


def arc_length(radius, angle):
    """ Длина дуги по радиусу и углу
    """
    return radius * angle


def chord_height(radius, angle):
    """ Высота хорды по радиусу и углу
    """
    return radius - math.cos(angle / 2) * radius


def overshoot(screen_height, chord_height, throw_distance):
    """ Высота засветки стены при изогнутом экране
    """
    return (screen_height / throw_distance) * chord_height


def line_angle(x1, y1, x2, y2):
    """ Угол между отрезком и осью X, в градусах
    """
    dx = x2 - x1
    dy = y2 - y1
    sdx, sdy = math.copysign(1.0, dx), math.copysign(1.0, dy)

    if dx == 0:
        angle = sdy * math.pi / 2
    else:
        angle = math.atan(dy / dx)

    angle = math.degrees(angle)

    angle = ((270 + sdx * 90) + angle) % 360

    return angle


def line_length(x1, y1, x2, y2):
    """ Длина отрезка
    """
    dx = x2 - x1
    dy = y2 - y1
    d_len = math.sqrt(dx ** 2 + dy ** 2)
    return d_len


def line_by_angle(x1, y1, angle, length):
    """ Координаты второго конца отрезка по координатам первого, углу от оси X и длине
    """
    rad_angle = math.radians(angle)
    x2 = math.cos(rad_angle) * length + x1
    y2 = math.sin(rad_angle) * length + y1
    return x2, y2


def inch_to_mm(inch):
    return inch * 25.4


def mm_to_inch(mm):
    return mm / 25.4


def get_real_blend(width, height, aspect, min_blend):
    """
    Calculate projection system for selected surface and with projector of desired aspect ratio.

    :param width: width of projection surface
    :param height: height of projection surface
    :param aspect: vertical part of aspect ratio (with horizontal based on 16, i.e. 4:3 is 12)
    :param min_blend: minimal blend width in percents
    :return: current aspect, blend in percents, blend in mm, and projectors count
    """
    screen_width = 16 * (height / aspect)
    blend_width = screen_width * (min_blend / 100)
    logger.info("16:{} w={}, h={}, b={}".format(aspect, screen_width, height, blend_width))

    screens_count = (width - blend_width) / (screen_width - blend_width)
    projectors_count = math.ceil(screens_count)

    # used formula calculated this way: (x is a real blend width)
    # (width-x)/(screen_width-x) = projectors_count
    # width-x = projectors_count*screen_width - projectors_count*x
    # width - projectors_count*screen_width = x - projectors_count*x
    if projectors_count == 1:
        # to avoid division on zero
        new_blend = screen_width
        blend_percent = 100
    else:
        new_blend = (width - projectors_count * screen_width) / (1 - projectors_count)
        blend_percent = (new_blend / screen_width) * 100

    logger.info("projectors={}, b={}, b%={}".format(projectors_count, new_blend, blend_percent))
    return aspect, blend_percent, new_blend, projectors_count
