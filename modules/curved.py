#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from collections import namedtuple

from PyQt5 import QtCore, QtGui, QtWidgets

from modules import curved_ui
from modules import calculate
from modules.schemes import Scheme

logger = logging.getLogger(__name__)

ScreenBlock = namedtuple("ScreenBlock", ("height", "chord", "radius", "chord_height", "angle"))
ProjectorBlock = namedtuple("ProjectorBlock", ("width", "depth", "height", "throw"))


class CylindricalScreenW(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.ui = curved_ui.Ui_CylindricalScreen()
        self.ui.setupUi(self)
        self.ui.gv_side.setScene(SideScheme())
        self.ui.gv_top.setScene(TopScheme())

    def refresh_data(self, radius, chord):
        try:
            ar, ad = calculate.arc_angle(radius, chord)
            self.ui.screenAngleRad.setValue(ar)
            self.ui.screenAngleDeg.setValue(ad)
            al = calculate.arc_length(radius, ar)
            self.ui.screenArcLength.setValue(int(al))
            ch = calculate.chord_height(radius, ar)
            self.ui.chordHeight.setValue(int(ch))
        except ValueError as err:
            print(err)

    def refresh_checkers(self, radius, distance, chord_height):
        if distance != 0:
            self.ui.aspect1.setValue(radius / distance)
        if distance + chord_height != 0:
            self.ui.aspect2.setValue(radius / (distance + chord_height))

    @QtCore.pyqtSlot()
    def on_calculate_screen_clicked(self):
        radius = self.ui.screenRadius.value()
        chord = self.ui.screenChord.value()
        ratio = self.ui.throwRatio.value()

        self.refresh_data(radius, chord)

        td = calculate.throw_distance(ratio, chord)
        self.ui.throwDistance.setValue(int(td))
        self.refresh_checkers(radius, td, self.ui.chordHeight.value())

    def prepare_text(self):
        surface = ScreenBlock(
            radius=self.ui.screenRadius.value(),
            height=self.ui.screenHeight.value(),
            chord=self.ui.screenChord.value(),
            chord_height=self.ui.chordHeight.value(),
            angle=self.ui.screenAngleDeg.value(),
        )
        ratio = self.ui.throwRatio.value()
        throw = self.ui.throwDistance.value()
        arc_length = self.ui.screenArcLength.value()
        aspect1 = self.ui.aspect1.value()
        aspect2 = self.ui.aspect2.value()

        text = ""
        with open("templates/cylindrical.html", "rt") as file:
            text = file.read()
            text = text.format(**vars())

        return text

    def get_document(self):
        editor = QtWidgets.QTextEdit()
        document = QtGui.QTextDocument()
        editor.setDocument(document)

        side_image = self.ui.gv_side.scene().get_image()
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/cs_side.png"), side_image)

        top_image = self.ui.gv_top.scene().get_image()
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/cs_top.png"), top_image)

        text = self.prepare_text()
        editor.append(text)
        document.end()

        return document

    @QtCore.pyqtSlot()
    def on_copy_data_clicked(self):
        print("Copy to clipboard:")
        document = self.get_document()

        data = QtCore.QMimeData()
        data.setHtml(document.toHtml())

        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setMimeData(data, QtGui.QClipboard.Clipboard)

    @QtCore.pyqtSlot()
    def on_print_data_clicked(self):
        print("Print dialog:")
        from PyQt5 import QtPrintSupport

        document = self.get_document()

        printer = QtPrintSupport.QPrinter()
        print_dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        print_dialog.paintRequested.connect(document.print)
        if print_dialog.exec() != print_dialog.Accepted:
            return

        document.print(printer)

    @QtCore.pyqtSlot()
    def on_draw_screen_clicked(self):
        print("Draw schemes:")

        surface = ScreenBlock(
            radius=self.ui.screenRadius.value(),
            height=self.ui.screenHeight.value(),
            chord=self.ui.screenChord.value(),
            chord_height=self.ui.chordHeight.value(),
            angle=self.ui.screenAngleDeg.value(),
        )
        print("surface:", surface)
        projector = ProjectorBlock(
            width=400, depth=400, height=200,
            throw=self.ui.throwDistance.value(),
        )
        print("projector:", projector)

        sizes_step_full = 30

        hs = CylindricalScreenW._get_horizontal_scale(
            surface.chord_height,
            surface.radius,
            projector.throw,
            projector[0],
            sizes_step_full,
            self.ui.gv_side.width()
        )
        vs1 = CylindricalScreenW._get_vertical_scale_side(
            surface.height,
            calculate.overshoot(surface.height, surface.chord_height, projector.throw),
            sizes_step_full,
            self.ui.gv_side.height()
        )
        vs2 = CylindricalScreenW._get_vertical_scale_top(
            surface.chord,
            sizes_step_full,
            self.ui.gv_top.height()
        )
        scale = min(hs, vs1, vs2)
        print("Scale selected as:", scale, (hs, vs1, vs2))

        side = self.ui.gv_side.scene()
        side.initializeItems(surface, projector, scale)

        top = self.ui.gv_top.scene()
        top.initializeItems(surface, projector, scale)

    def _get_horizontal_scale(screen, radius, throw_distance, projector, sizes, view_size):
        # Полная ширина = экран + макс(радиус, проекционное расстояние) + проектор + высотные размеры;
        return (view_size - sizes) / (screen + max(radius, throw_distance) + projector)

    def _get_vertical_scale_side(screen, overshoot, sizes, view_size):
        # Полная высота(вид сбоку) = высота экрана + высота засветки + горизонтальные размеры;
        return (view_size - sizes) / (screen + overshoot)

    def _get_vertical_scale_top(screen, sizes, view_size):
        # Полная высота(вид сверху) = длина хорды + горизонтальные размеры;
        return (view_size - sizes) / screen


# Функции и объекты отрисовки
class SideScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.surface = None
        self.projection = None
        self.projector = None
        self.annotations = None

        self.resetItems()

    def resetItems(self):
        self.surface = None
        self.projection = []
        self.projector = None
        self.annotations = []

    def initializeItems(self, surface, projector, scale=1.0):
        self.clear()
        self.resetItems()

        # draw surface
        screen_h = scale * surface.height
        screen_w = scale * surface.chord_height
        projector_x = scale * (projector.throw + surface.chord_height)
        over_unscaled = calculate.overshoot(surface.height, surface.chord_height, projector.throw)
        overshoot = scale * over_unscaled

        self.surface = self.addRect(0, 0, screen_w, screen_h, pen=self.pen_surface)

        self.projector = self.addRect(
            projector_x, 0,
            scale * projector.depth, scale * projector.height, pen=self.pen_projector)

        line = self.addLine(0, 0, projector_x, 0, pen=self.pen_projection)
        self.projection.append(line)

        line = self.addLine(0, screen_h + overshoot, projector_x, 0, pen=self.pen_projection)
        self.projection.append(line)

        line = self.addLine(0, screen_h, 0, screen_h + overshoot, pen=self.pen_illusion)
        self.annotations.append(line)

        d = self.drawHorizontalDimension(0, 0, screen_w, 0, -15, "{:d}".format(surface.chord_height))
        self.annotations.append(d)
        d = self.drawHorizontalDimension(screen_w, 0, projector_x, 0, -15, "{:d}".format(projector.throw))
        self.annotations.append(d)
        d = self.drawHorizontalDimension(projector_x, 0, projector_x + scale * projector.depth, 0, -15,
                                         "{:d}".format(projector.depth))
        self.annotations.append(d)

        d = self.drawVerticalDimension(0, screen_h, 0, 0, -15, "{:d}".format(surface.height))
        self.annotations.append(d)
        d = self.drawVerticalDimension(0, screen_h + overshoot, 0, screen_h, -15, "{:d}".format(int(over_unscaled)))
        self.annotations.append(d)


class TopScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.surface = None
        self.projection = None
        self.projector = None
        self.annotations = None

        self.resetItems()

    def resetItems(self):
        self.surface = None
        self.projection = []
        self.projector = None
        self.annotations = []

    def initializeItems(self, surface, projector, scale=1.0):
        self.clear()
        self.resetItems()

        # draw surface
        screen_h_2 = scale * surface.chord / 2
        screen_w = scale * surface.chord_height
        screen_r = scale * surface.radius
        screen_d = screen_r * 2
        screen_start_angle = 180.0 - (surface.angle / 2)
        projector_x = scale * (projector.throw + surface.chord_height)
        projector_y_2 = scale * (projector.width) / 2

        arc = QtGui.QPainterPath()
        arc.arcMoveTo(0, -screen_r, screen_d, screen_d, screen_start_angle)
        arc.arcTo(0, -screen_r, screen_d, screen_d, screen_start_angle, surface.angle)
        self.surface = self.addPath(arc, pen=self.pen_surface)

        self.projector = self.addRect(
            projector_x, -projector_y_2,
            scale * projector.depth, projector_y_2 * 2, pen=self.pen_projector)

        line = self.addLine(screen_w, -screen_h_2, projector_x, 0, pen=self.pen_projection)
        self.projection.append(line)

        line = self.addLine(screen_w, screen_h_2, projector_x, 0, pen=self.pen_projection)
        self.projection.append(line)

        line = self.addLine(-5, 0, projector_x + scale * projector.depth + 5, 0, pen=self.pen_symmetry)
        self.annotations.append(line)

        d = self.drawHorizontalDimension(0, 0, projector_x, 0, -screen_h_2 - 15,
                                         "{:d}".format(surface.chord_height + projector.throw))
        self.annotations.append(d)

        d = self.drawHorizontalDimension(screen_w, -screen_h_2, projector_x, 0, -screen_h_2 + 15,
                                         "{:d}".format(projector.throw))
        self.annotations.append(d)

        d = self.drawRadiusDimension(screen_r, 0, screen_r, 180 - surface.angle / 2 + surface.angle / 5, 100,
                                     "R {:d}".format(surface.radius))
        self.annotations.append(d)

        d = self.drawVerticalDimension(screen_w, screen_h_2, screen_w, -screen_h_2, -15, "{:d}".format(surface.chord))
        self.annotations.append(d)
