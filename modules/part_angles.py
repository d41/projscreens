#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
from PyQt5 import QtCore, QtWidgets
from . import part_angles_ui


class PartAnglesW(QtWidgets.QWidget):
    def __init__(self):
        super(PartAnglesW, self).__init__()
        self.ui = part_angles_ui.Ui_PartAngles()
        self.ui.setupUi(self)

        self.setup_resolutions()
        self.on_screen_resolution_currentIndexChanged(0)

        self.ui.screen_width.editingFinished.connect(self.calculate_width)
        self.ui.screen_height.editingFinished.connect(self.calculate_height)
        self.ui.screen_resolution_h.editingFinished.connect(self.calculate_width)
        self.ui.screen_resolution_v.editingFinished.connect(self.calculate_height)

        self.ui.distance.editingFinished.connect(self.calculateSizes)

    def setup_resolutions(self):
        for index in range(0, self.ui.screen_resolution.count()):
            text = self.ui.screen_resolution.itemText(index)
            text = text.split(' ')[0]
            horizontal, vertical = map(int, text.split('x'))
            self.ui.screen_resolution.setItemData(index, (horizontal, vertical))

    @QtCore.pyqtSlot(int)
    def on_screen_resolution_currentIndexChanged(self, index):
        h, v = self.ui.screen_resolution.itemData(index)
        self.ui.screen_resolution_h.setValue(h)
        self.ui.screen_resolution_v.setValue(v)

    def calculate_width(self):
        self.ui.pixel_width.setValue(self.ui.screen_width.value() / (self.ui.screen_resolution_h.value() or 1))

    def calculate_height(self):
        self.ui.pixel_height.setValue(self.ui.screen_height.value() / (self.ui.screen_resolution_v.value() or 1))

    def calculateSizes(self):
        size_rad = math.atan(self.ui.pixel_height.value() / self.ui.distance.value())
        size_deg = math.degrees(size_rad)
        size_minutes = size_deg * 60.0

        self.ui.pixel_minutes.setValue(size_minutes)
        if size_minutes > 0:
            self.ui.size21.setValue(21 / size_minutes)
            self.ui.size35.setValue(35 / size_minutes)
            self.ui.size6.setValue(6 / size_minutes)

        size_rad = math.atan(self.ui.screen_width.value() / self.ui.distance.value())
        size_deg = math.degrees(size_rad)
        self.ui.angle_H.setValue(size_deg)

        size_rad = math.atan(self.ui.screen_height.value() / self.ui.distance.value())
        size_deg = math.degrees(size_rad)
        self.ui.angle_V.setValue(size_deg)


def __main():
    app = QtWidgets.QApplication(sys.argv)
    win = PartAnglesW()
    win.show()
    sys.exit(app.exec_())


if "__main__" == __name__:
    __main()
