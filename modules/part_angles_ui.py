# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interfaces/part_angles.ui'
#
# Created by: PyQt5 UI code generator 5.15.8
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PartAngles(object):
    def setupUi(self, PartAngles):
        PartAngles.setObjectName("PartAngles")
        PartAngles.resize(442, 878)
        self.verticalLayout = QtWidgets.QVBoxLayout(PartAngles)
        self.verticalLayout.setObjectName("verticalLayout")
        self.group_pixels = QtWidgets.QGroupBox(PartAngles)
        self.group_pixels.setObjectName("group_pixels")
        self.formLayout = QtWidgets.QFormLayout(self.group_pixels)
        self.formLayout.setObjectName("formLayout")
        self.label_width = QtWidgets.QLabel(self.group_pixels)
        self.label_width.setObjectName("label_width")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_width)
        self.screen_width = QtWidgets.QSpinBox(self.group_pixels)
        self.screen_width.setMaximum(10000)
        self.screen_width.setSingleStep(10)
        self.screen_width.setObjectName("screen_width")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.screen_width)
        self.label_height = QtWidgets.QLabel(self.group_pixels)
        self.label_height.setObjectName("label_height")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_height)
        self.screen_height = QtWidgets.QSpinBox(self.group_pixels)
        self.screen_height.setMaximum(10000)
        self.screen_height.setSingleStep(10)
        self.screen_height.setObjectName("screen_height")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.screen_height)
        self.label_resolution_h = QtWidgets.QLabel(self.group_pixels)
        self.label_resolution_h.setObjectName("label_resolution_h")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_resolution_h)
        self.screen_resolution_h = QtWidgets.QSpinBox(self.group_pixels)
        self.screen_resolution_h.setMaximum(10000)
        self.screen_resolution_h.setObjectName("screen_resolution_h")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.screen_resolution_h)
        self.label_resolution_v = QtWidgets.QLabel(self.group_pixels)
        self.label_resolution_v.setObjectName("label_resolution_v")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_resolution_v)
        self.screen_resolution_v = QtWidgets.QSpinBox(self.group_pixels)
        self.screen_resolution_v.setMaximum(10000)
        self.screen_resolution_v.setObjectName("screen_resolution_v")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.screen_resolution_v)
        self.label_resolution = QtWidgets.QLabel(self.group_pixels)
        self.label_resolution.setObjectName("label_resolution")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_resolution)
        self.screen_resolution = QtWidgets.QComboBox(self.group_pixels)
        self.screen_resolution.setObjectName("screen_resolution")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.screen_resolution.addItem("")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.screen_resolution)
        self.line = QtWidgets.QFrame(self.group_pixels)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.SpanningRole, self.line)
        self.label_pixel_height = QtWidgets.QLabel(self.group_pixels)
        self.label_pixel_height.setObjectName("label_pixel_height")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label_pixel_height)
        self.label_pixel_width = QtWidgets.QLabel(self.group_pixels)
        self.label_pixel_width.setObjectName("label_pixel_width")
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.LabelRole, self.label_pixel_width)
        self.label_notes = QtWidgets.QLabel(self.group_pixels)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.label_notes.sizePolicy().hasHeightForWidth())
        self.label_notes.setSizePolicy(sizePolicy)
        self.label_notes.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_notes.setWordWrap(True)
        self.label_notes.setObjectName("label_notes")
        self.formLayout.setWidget(8, QtWidgets.QFormLayout.SpanningRole, self.label_notes)
        self.pixel_width = QtWidgets.QDoubleSpinBox(self.group_pixels)
        self.pixel_width.setMaximum(9999.99)
        self.pixel_width.setObjectName("pixel_width")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.pixel_width)
        self.pixel_height = QtWidgets.QDoubleSpinBox(self.group_pixels)
        self.pixel_height.setMaximum(9999.99)
        self.pixel_height.setObjectName("pixel_height")
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.pixel_height)
        self.verticalLayout.addWidget(self.group_pixels)
        self.group_minutes = QtWidgets.QGroupBox(PartAngles)
        self.group_minutes.setObjectName("group_minutes")
        self.formLayout_2 = QtWidgets.QFormLayout(self.group_minutes)
        self.formLayout_2.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout_2.setObjectName("formLayout_2")
        self.line_2 = QtWidgets.QFrame(self.group_minutes)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.SpanningRole, self.line_2)
        self.label_minutes = QtWidgets.QLabel(self.group_minutes)
        self.label_minutes.setObjectName("label_minutes")
        self.formLayout_2.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label_minutes)
        self.pixel_minutes = QtWidgets.QDoubleSpinBox(self.group_minutes)
        self.pixel_minutes.setReadOnly(True)
        self.pixel_minutes.setSuffix("")
        self.pixel_minutes.setMaximum(9999.99)
        self.pixel_minutes.setObjectName("pixel_minutes")
        self.formLayout_2.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.pixel_minutes)
        self.label_size21 = QtWidgets.QLabel(self.group_minutes)
        self.label_size21.setObjectName("label_size21")
        self.formLayout_2.setWidget(7, QtWidgets.QFormLayout.LabelRole, self.label_size21)
        self.size21 = QtWidgets.QDoubleSpinBox(self.group_minutes)
        self.size21.setReadOnly(True)
        self.size21.setDecimals(2)
        self.size21.setMaximum(9999.99)
        self.size21.setObjectName("size21")
        self.formLayout_2.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.size21)
        self.label_size35 = QtWidgets.QLabel(self.group_minutes)
        self.label_size35.setObjectName("label_size35")
        self.formLayout_2.setWidget(8, QtWidgets.QFormLayout.LabelRole, self.label_size35)
        self.size35 = QtWidgets.QDoubleSpinBox(self.group_minutes)
        self.size35.setReadOnly(True)
        self.size35.setObjectName("size35")
        self.formLayout_2.setWidget(8, QtWidgets.QFormLayout.FieldRole, self.size35)
        self.label_size6 = QtWidgets.QLabel(self.group_minutes)
        self.label_size6.setObjectName("label_size6")
        self.formLayout_2.setWidget(9, QtWidgets.QFormLayout.LabelRole, self.label_size6)
        self.size6 = QtWidgets.QDoubleSpinBox(self.group_minutes)
        self.size6.setReadOnly(True)
        self.size6.setObjectName("size6")
        self.formLayout_2.setWidget(9, QtWidgets.QFormLayout.FieldRole, self.size6)
        self.label_notes_2 = QtWidgets.QLabel(self.group_minutes)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.label_notes_2.sizePolicy().hasHeightForWidth())
        self.label_notes_2.setSizePolicy(sizePolicy)
        self.label_notes_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_notes_2.setWordWrap(True)
        self.label_notes_2.setObjectName("label_notes_2")
        self.formLayout_2.setWidget(10, QtWidgets.QFormLayout.SpanningRole, self.label_notes_2)
        self.label_angle_V = QtWidgets.QLabel(self.group_minutes)
        self.label_angle_V.setObjectName("label_angle_V")
        self.formLayout_2.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_angle_V)
        self.label_angle_H = QtWidgets.QLabel(self.group_minutes)
        self.label_angle_H.setObjectName("label_angle_H")
        self.formLayout_2.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_angle_H)
        self.angle_V = QtWidgets.QDoubleSpinBox(self.group_minutes)
        self.angle_V.setReadOnly(True)
        self.angle_V.setMaximum(360.0)
        self.angle_V.setObjectName("angle_V")
        self.formLayout_2.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.angle_V)
        self.angle_H = QtWidgets.QDoubleSpinBox(self.group_minutes)
        self.angle_H.setReadOnly(True)
        self.angle_H.setMaximum(360.0)
        self.angle_H.setObjectName("angle_H")
        self.formLayout_2.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.angle_H)
        self.distance = QtWidgets.QSpinBox(self.group_minutes)
        self.distance.setMinimum(10)
        self.distance.setMaximum(20000)
        self.distance.setSingleStep(10)
        self.distance.setProperty("value", 3000)
        self.distance.setObjectName("distance")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.distance)
        self.label_distance = QtWidgets.QLabel(self.group_minutes)
        self.label_distance.setObjectName("label_distance")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_distance)
        self.verticalLayout.addWidget(self.group_minutes)
        self.verticalLayout.setStretch(1, 1)
        self.label_width.setBuddy(self.screen_width)
        self.label_height.setBuddy(self.screen_height)
        self.label_resolution_h.setBuddy(self.screen_resolution_h)
        self.label_resolution_v.setBuddy(self.screen_resolution_v)
        self.label_resolution.setBuddy(self.screen_resolution)
        self.label_pixel_height.setBuddy(self.pixel_width)
        self.label_pixel_width.setBuddy(self.pixel_height)
        self.label_minutes.setBuddy(self.pixel_minutes)
        self.label_size21.setBuddy(self.size21)
        self.label_size35.setBuddy(self.size35)
        self.label_size6.setBuddy(self.size6)
        self.label_angle_V.setBuddy(self.angle_V)
        self.label_angle_H.setBuddy(self.angle_H)
        self.label_distance.setBuddy(self.distance)

        self.retranslateUi(PartAngles)
        QtCore.QMetaObject.connectSlotsByName(PartAngles)
        PartAngles.setTabOrder(self.screen_width, self.screen_height)
        PartAngles.setTabOrder(self.screen_height, self.screen_resolution)
        PartAngles.setTabOrder(self.screen_resolution, self.screen_resolution_h)
        PartAngles.setTabOrder(self.screen_resolution_h, self.screen_resolution_v)
        PartAngles.setTabOrder(self.screen_resolution_v, self.pixel_width)
        PartAngles.setTabOrder(self.pixel_width, self.pixel_height)
        PartAngles.setTabOrder(self.pixel_height, self.distance)
        PartAngles.setTabOrder(self.distance, self.angle_H)
        PartAngles.setTabOrder(self.angle_H, self.angle_V)
        PartAngles.setTabOrder(self.angle_V, self.pixel_minutes)
        PartAngles.setTabOrder(self.pixel_minutes, self.size21)
        PartAngles.setTabOrder(self.size21, self.size35)
        PartAngles.setTabOrder(self.size35, self.size6)

    def retranslateUi(self, PartAngles):
        _translate = QtCore.QCoreApplication.translate
        PartAngles.setWindowTitle(_translate("PartAngles", "Угловые размеры"))
        self.group_pixels.setTitle(_translate("PartAngles", "Абсолютные размеры"))
        self.label_width.setText(_translate("PartAngles", "Ширина изображения"))
        self.screen_width.setSuffix(_translate("PartAngles", " мм"))
        self.label_height.setText(_translate("PartAngles", "Высота изображения"))
        self.screen_height.setSuffix(_translate("PartAngles", " мм"))
        self.label_resolution_h.setText(_translate("PartAngles", "Разрешение, горизонталь"))
        self.label_resolution_v.setText(_translate("PartAngles", "Разрешение, вертикаль"))
        self.label_resolution.setText(_translate("PartAngles", "Разрешение"))
        self.screen_resolution.setItemText(0, _translate("PartAngles", "1920x1200 (WUXGA)"))
        self.screen_resolution.setItemText(1, _translate("PartAngles", "1680x1050 (WSXGA+)"))
        self.screen_resolution.setItemText(2, _translate("PartAngles", "1280x800 (WXGA)"))
        self.screen_resolution.setItemText(3, _translate("PartAngles", "1920x1080 (FHD)"))
        self.screen_resolution.setItemText(4, _translate("PartAngles", "1600x900 (11)"))
        self.screen_resolution.setItemText(5, _translate("PartAngles", "1366x768 (NT)"))
        self.screen_resolution.setItemText(6, _translate("PartAngles", "1280x720 (HD/WXGA)"))
        self.screen_resolution.setItemText(7, _translate("PartAngles", "854x480 (FWVGA)"))
        self.screen_resolution.setItemText(8, _translate("PartAngles", "1600x1200 (UXGA)"))
        self.screen_resolution.setItemText(9, _translate("PartAngles", "1400x1050 (SXGA+)"))
        self.screen_resolution.setItemText(10, _translate("PartAngles", "1024x768 (XGA)"))
        self.screen_resolution.setItemText(11, _translate("PartAngles", "800x600 (SVGA)"))
        self.label_pixel_height.setText(_translate("PartAngles", "Пиксель, ширина"))
        self.label_pixel_width.setText(_translate("PartAngles", "Пиксель, высота"))
        self.label_notes.setText(_translate("PartAngles", "Получение физических размеров пикселей по разрешению и размерам экрана. Используется для дальнейшей работы с угловыми минутами."))
        self.pixel_width.setSuffix(_translate("PartAngles", " мм"))
        self.pixel_height.setSuffix(_translate("PartAngles", " мм"))
        self.group_minutes.setTitle(_translate("PartAngles", "Угловые размеры"))
        self.label_minutes.setText(_translate("PartAngles", "Размер в уголовых минутах"))
        self.label_size21.setText(_translate("PartAngles", "&21\' - Простой элемент мнемосхемы"))
        self.size21.setSuffix(_translate("PartAngles", " пикс."))
        self.label_size35.setText(_translate("PartAngles", "&35\' - Составной элемент мнемосхемы"))
        self.size35.setSuffix(_translate("PartAngles", " пикс."))
        self.label_size6.setText(_translate("PartAngles", "&6\' - Часть составного элемента"))
        self.size6.setSuffix(_translate("PartAngles", " пикс."))
        self.label_notes_2.setText(_translate("PartAngles", "Минимально различимый глазом объект имеет размеры 0,34 угловых минуты. В качестве расчетного используем примерно 0,40. Требования к размерам мнемосхем взяты из каких-то ГОСТов от диспетчерских и пультов АСУТП. По ощущениям размер точки должен быть 1,0-1,5 для того чтобы не сильно напрягаясь читать цифры, меньше - смысла нет."))
        self.label_angle_V.setText(_translate("PartAngles", "Высота изображения"))
        self.label_angle_H.setText(_translate("PartAngles", "Ширина изображения"))
        self.angle_V.setSuffix(_translate("PartAngles", " гр."))
        self.angle_H.setSuffix(_translate("PartAngles", " гр."))
        self.distance.setSuffix(_translate("PartAngles", " мм"))
        self.label_distance.setText(_translate("PartAngles", "Расстояние до экрана"))
