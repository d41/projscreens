#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
from PyQt5 import QtCore, QtWidgets
from modules import part_brightness_ui


class PartBrightnessW(QtWidgets.QWidget):
    # aspects = ((16,10), (4,3), (16,9))
    aspects = ((16, 10), (16, 12), (16, 9))
    send_data = QtCore.pyqtSignal(str)

    def __init__(self):
        super(PartBrightnessW, self).__init__()

        self.ui = part_brightness_ui.Ui_PartBrightness()
        self.ui.setupUi(self)

        self.original = self.ui.original.value()
        self.Khave = None
        self.Kwant = None
        self.area = self.ui.screen_area.value()

        self.setHave(0)
        self.setWant(0)

        self.ui.original.editingFinished.connect(self.setOriginal)
        self.ui.aspect_have.currentIndexChanged.connect(self.setHave)
        self.ui.aspect_want.currentIndexChanged.connect(self.setWant)
        self.ui.screen_area.editingFinished.connect(self.setArea)

    def setHave(self, index):
        self.Khave = self.aspects[index]
        self.calculate()

    def setWant(self, index):
        self.Kwant = self.aspects[index]
        self.calculate()

    def setOriginal(self):
        self.original = self.ui.original.value()
        self.calculate()

    def setArea(self):
        self.area = self.ui.screen_area.value()
        self.calculate()

    def calculate(self):
        if self.Khave is None or self.Kwant is None:
            self.ui.resulting.setValue(0)
            return

        # TODO: Потом заменить.
        # Грязный чит: не поддерживает произвольные форматы, а только три заданных.
        # Из-за того что приведены по горизонтали к общему значению.
        Khh, Khv = self.Khave
        Kwh, Kwv = self.Kwant
        brightness = 0

        if Kwv == Khv:
            brightness = self.original
        elif Kwv > Khv:
            K = Khv / Kwv
            brightness = self.original * K
        else:
            K = Kwv / Khv
            brightness = self.original * K

        self.ui.resulting.setValue(int(brightness))
        self.ui.brightness.setValue(int(brightness / self.area))

    @QtCore.pyqtSlot()
    def on_button_send_clicked(self):
        translate = QtCore.QCoreApplication.translate
        values = dict()
        values["aspect_have"] = self.ui.aspect_have.currentText()
        values["aspect_want"] = self.ui.aspect_want.currentText()
        values["brightness_before"] = self.ui.original.value()
        values["brightness_after"] = self.ui.resulting.value()
        values["area"] = self.area
        values["brightness"] = self.ui.brightness.value()

        template = translate("PartBrightnessW", """\
<h1>Brightness</h1>
<h2>Projector</h2>
<ul>
<li>aspect ratio {aspect_have};</li>
<li>brightness {brightness_before} lm.</li>
</ul>

<h2>Screen</h2>
<ul>
<li>aspect ratio {aspect_want};</li>
<li>usefull illuminance {brightness_after} lm / {area} m2 = {brightness} lx (or lm/m2).</li>
</ul>
<p><br/></p>
""")
        text = template.format(**values)
        self.send_data.emit(text)
