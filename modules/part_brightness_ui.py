# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interfaces/part_brightness.ui'
#
# Created by: PyQt5 UI code generator 5.15.8
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PartBrightness(object):
    def setupUi(self, PartBrightness):
        PartBrightness.setObjectName("PartBrightness")
        PartBrightness.resize(416, 421)
        self.verticalLayout = QtWidgets.QVBoxLayout(PartBrightness)
        self.verticalLayout.setObjectName("verticalLayout")
        self.group_brightness = QtWidgets.QGroupBox(PartBrightness)
        self.group_brightness.setObjectName("group_brightness")
        self.formLayout = QtWidgets.QFormLayout(self.group_brightness)
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label_original = QtWidgets.QLabel(self.group_brightness)
        self.label_original.setObjectName("label_original")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_original)
        self.label_aspect_have = QtWidgets.QLabel(self.group_brightness)
        self.label_aspect_have.setObjectName("label_aspect_have")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_aspect_have)
        self.label_aspect_want = QtWidgets.QLabel(self.group_brightness)
        self.label_aspect_want.setObjectName("label_aspect_want")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_aspect_want)
        self.aspect_have = QtWidgets.QComboBox(self.group_brightness)
        self.aspect_have.setObjectName("aspect_have")
        self.aspect_have.addItem("")
        self.aspect_have.setItemText(0, "16:10")
        self.aspect_have.addItem("")
        self.aspect_have.setItemText(1, "4:3")
        self.aspect_have.addItem("")
        self.aspect_have.setItemText(2, "16:9")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.aspect_have)
        self.aspect_want = QtWidgets.QComboBox(self.group_brightness)
        self.aspect_want.setObjectName("aspect_want")
        self.aspect_want.addItem("")
        self.aspect_want.setItemText(0, "16:10")
        self.aspect_want.addItem("")
        self.aspect_want.setItemText(1, "4:3")
        self.aspect_want.addItem("")
        self.aspect_want.setItemText(2, "16:9")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.aspect_want)
        self.original = QtWidgets.QSpinBox(self.group_brightness)
        self.original.setMinimum(100)
        self.original.setMaximum(30000)
        self.original.setSingleStep(100)
        self.original.setProperty("value", 3000)
        self.original.setObjectName("original")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.original)
        self.line = QtWidgets.QFrame(self.group_brightness)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.SpanningRole, self.line)
        self.label_resulting = QtWidgets.QLabel(self.group_brightness)
        self.label_resulting.setObjectName("label_resulting")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_resulting)
        self.resulting = QtWidgets.QSpinBox(self.group_brightness)
        self.resulting.setReadOnly(True)
        self.resulting.setMaximum(30000)
        self.resulting.setObjectName("resulting")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.resulting)
        self.label_notes = QtWidgets.QLabel(self.group_brightness)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.label_notes.sizePolicy().hasHeightForWidth())
        self.label_notes.setSizePolicy(sizePolicy)
        self.label_notes.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_notes.setWordWrap(True)
        self.label_notes.setObjectName("label_notes")
        self.formLayout.setWidget(8, QtWidgets.QFormLayout.SpanningRole, self.label_notes)
        self.button_send = QtWidgets.QPushButton(self.group_brightness)
        self.button_send.setObjectName("button_send")
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.button_send)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout.setItem(7, QtWidgets.QFormLayout.LabelRole, spacerItem)
        self.label_area = QtWidgets.QLabel(self.group_brightness)
        self.label_area.setObjectName("label_area")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_area)
        self.screen_area = QtWidgets.QDoubleSpinBox(self.group_brightness)
        self.screen_area.setMinimum(0.01)
        self.screen_area.setMaximum(500.0)
        self.screen_area.setSingleStep(0.01)
        self.screen_area.setProperty("value", 2.5)
        self.screen_area.setObjectName("screen_area")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.screen_area)
        self.label_brightness = QtWidgets.QLabel(self.group_brightness)
        self.label_brightness.setObjectName("label_brightness")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label_brightness)
        self.brightness = QtWidgets.QSpinBox(self.group_brightness)
        self.brightness.setMaximum(10000)
        self.brightness.setObjectName("brightness")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.brightness)
        self.verticalLayout.addWidget(self.group_brightness)
        self.label_original.setBuddy(self.original)
        self.label_aspect_have.setBuddy(self.aspect_have)
        self.label_aspect_want.setBuddy(self.aspect_want)
        self.label_resulting.setBuddy(self.resulting)
        self.label_area.setBuddy(self.screen_area)
        self.label_brightness.setBuddy(self.brightness)

        self.retranslateUi(PartBrightness)
        QtCore.QMetaObject.connectSlotsByName(PartBrightness)
        PartBrightness.setTabOrder(self.original, self.aspect_have)
        PartBrightness.setTabOrder(self.aspect_have, self.aspect_want)
        PartBrightness.setTabOrder(self.aspect_want, self.screen_area)
        PartBrightness.setTabOrder(self.screen_area, self.resulting)
        PartBrightness.setTabOrder(self.resulting, self.brightness)
        PartBrightness.setTabOrder(self.brightness, self.button_send)

    def retranslateUi(self, PartBrightness):
        _translate = QtCore.QCoreApplication.translate
        PartBrightness.setWindowTitle(_translate("PartBrightness", "Brightness correction"))
        self.group_brightness.setTitle(_translate("PartBrightness", "Values"))
        self.label_original.setText(_translate("PartBrightness", "Projector brightness"))
        self.label_aspect_have.setText(_translate("PartBrightness", "Projector\'s aspect ratio"))
        self.label_aspect_want.setText(_translate("PartBrightness", "Screen\'s aspect ratio"))
        self.original.setSuffix(_translate("PartBrightness", " lm"))
        self.label_resulting.setText(_translate("PartBrightness", "Corrected brightness"))
        self.resulting.setSuffix(_translate("PartBrightness", " lm"))
        self.label_notes.setText(_translate("PartBrightness", "If screen\'s and projector\'s aspect ratios doesn\'t match than there is losses of usefull light."))
        self.button_send.setText(_translate("PartBrightness", "Add to notes"))
        self.label_area.setText(_translate("PartBrightness", "Area"))
        self.screen_area.setSuffix(_translate("PartBrightness", " m2"))
        self.label_brightness.setText(_translate("PartBrightness", "Illuminance"))
        self.brightness.setSuffix(_translate("PartBrightness", " lm/m2"))
