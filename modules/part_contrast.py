#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtWidgets
from modules import part_contrast_ui


class PartContrastW(QtWidgets.QWidget):
    send_data = QtCore.pyqtSignal(str)

    def __init__(self):
        super(PartContrastW, self).__init__()

        self.ui = part_contrast_ui.Ui_PartContrast()
        self.ui.setupUi(self)

    @QtCore.pyqtSlot(int)
    def on_light_parasite_valueChanged(self, value):
        self.calculate()
        self.calculate_screen()

    @QtCore.pyqtSlot(int)
    def on_contrast_valueChanged(self, value):
        self.calculate()
        self.calculate_screen()

    @QtCore.pyqtSlot(float)
    def on_screen_area_valueChanged(self, value):
        self.calculate_screen()

    def calculate(self):
        self.ui.light_required.setValue(int(
            self.ui.light_parasite.value() * \
            self.ui.contrast.value() - \
            self.ui.light_parasite.value()))

    def calculate_screen(self):
        self.ui.light_screen.setValue(int(
            self.ui.light_required.value() * \
            self.ui.screen_area.value()))
