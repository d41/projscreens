#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtWidgets
from modules import part_distance_ui


class PartDistanceW(QtWidgets.QWidget):
    send_data = QtCore.pyqtSignal(str)

    def __init__(self):
        super(PartDistanceW, self).__init__()

        self.ui = part_distance_ui.Ui_PartDistance()
        self.ui.setupUi(self)

        self.direction = 0

        self.ui.screen.editingFinished.connect(self.calculate)
        self.ui.ratio.editingFinished.connect(self.calculate)
        self.ui.distance.editingFinished.connect(self.calculate)

    def calculate(self):
        if self.direction == 0:
            self.ui.distance.setValue(int(self.ui.screen.value() * self.ui.ratio.value()))
        elif self.direction == 1:
            self.ui.ratio.setValue(self.ui.distance.value() / self.ui.screen.value())
        elif self.direction == 2:
            self.ui.screen.setValue(int(self.ui.distance.value() / self.ui.ratio.value()))

    @QtCore.pyqtSlot()
    def on_calc_screen_clicked(self):
        self.direction = 2

    @QtCore.pyqtSlot()
    def on_calc_ratio_clicked(self):
        self.direction = 1

    @QtCore.pyqtSlot()
    def on_calc_distance_clicked(self):
        self.direction = 0

    @QtCore.pyqtSlot()
    def on_button_send_clicked(self):
        translate = QtCore.QCoreApplication.translate
        values = dict()
        values["screen"] = self.ui.screen.value()
        values["distance"] = self.ui.distance.value()
        values["ratio"] = self.ui.ratio.value()

        templates = [
            translate(
                "PartDistanceW",
                "Image on screen with width {screen} mm can be projected by projector with throw ratio {ratio} from distance {distance} mm."),
            translate(
                "PartDistanceW",
                "Image on screen with width {screen} mm can be projected from distance {distance} mm by projector with throw ratio {ratio}."),
            translate(
                "PartDistanceW",
                "Projector with throw ratio {ratio} from distance {distance} mm can create image on screen with width {screen} mm.")
        ]
        frame = translate("PartDistanceW", """<h1>Projection</h1><p>%s</p><br/>""")
        template = templates[self.direction]
        text = template.format(**values)
        self.send_data.emit(frame % text)
