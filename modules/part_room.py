#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtWidgets
from modules import part_room_ui


class PartRoomW(QtWidgets.QWidget):
    send_data = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(PartRoomW, self).__init__(parent=parent)

        self.ui = part_room_ui.Ui_PartRoom()
        self.ui.setupUi(self)

        self.ui.room_height.editingFinished.connect(self.update_limit_height)  #
        self.ui.room_screen.editingFinished.connect(self.update_limit_height)  #
        self.ui.room_length.editingFinished.connect(self.update_limit_diagonal)
        # self.connect(self.ui.room_width, QtCore.pyqtSignal("editingFinished()"), self.update_screen_diagonal_inch)
        self.ui.viewer_first.editingFinished.connect(self.update_limit_max)
        self.ui.viewer_last.editingFinished.connect(self.update_limit_min)
        self.ui.system_k.currentIndexChanged.connect(self.get_multiplier)

        self._multiplier = 6

    def get_multiplier(self, index):
        self._multiplier = (8, 6, 4)[index]
        self.update_limit_min()

    def update_limit_height(self):
        self.ui.limit_height.setValue(self.ui.room_height.value() - self.ui.room_screen.value())

    def update_limit_diagonal(self):
        self.ui.limit_diagonal.setValue(int(self.ui.room_length.value() / 2))

    def update_limit_max(self):
        self.ui.limit_max.setValue(int(self.ui.viewer_first.value() / 2))

    def update_limit_min(self):
        self.ui.limit_min.setValue(int(self.ui.viewer_last.value() / self._multiplier))

    @QtCore.pyqtSlot()
    def on_button_send_clicked(self):
        translate = QtCore.QCoreApplication.translate

        values = dict()
        values["width"] = self.ui.room_width.value()
        values["length"] = self.ui.room_length.value()
        values["height"] = self.ui.room_height.value()

        values["first"] = self.ui.viewer_first.value()
        values["last"] = self.ui.viewer_last.value()
        values["multiplier"] = self._multiplier

        values["physics"] = self.ui.limit_height.value()
        values["minimal"] = self.ui.limit_min.value()
        values["maximal"] = self.ui.limit_max.value()
        values["diagonal"] = self.ui.limit_diagonal.value()

        if values["minimal"] > values["maximal"]:
            values["comment"] = translate(
                "PartRoomW",
                """<em>N.B.: Limits looks weird! Check the need for doubling displays/screens.s</em>""")
        else:
            values["comment"] = ""

        template = translate(
            "PartRoomW",
            """\
<h1>Room</h1>
<h2>Sizes</h2>
<ul>
<li>width: {width} mm;</li>
<li>length: {length} mm;</li>
<li>height: {height} mm.</li>
</ul>

<h2>Placement</h2>
<ul>
<li>nearest viewer: {first} mm;</li>
<li>farthest viewer: {last} mm;</li>
<li>use factor multiplier: {multiplier}.</li>
</ul>

<h2>Screen</h2>
<ul>
<li>height limit: {physics} mm;</li>
<li>screen height from {minimal} to {maximal} mm;</li>
<li>maximal diagonal: {diagonal} mm.</li>
</ul>

<p>{comment}<br/>
</p>
""")
        text = template.format(**values)
        self.send_data.emit(text)
