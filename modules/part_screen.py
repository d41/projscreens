#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import sqrt
from PyQt5 import QtCore, QtWidgets
from modules import part_screen_ui


class PartScreenW(QtWidgets.QWidget):
    send_data = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(PartScreenW, self).__init__(parent=parent)

        self.ui = part_screen_ui.Ui_PartScreen()
        self.ui.setupUi(self)

        self.update_screen_aspect(0)

        self.ui.screen_aspect.currentIndexChanged.connect(self.update_screen_aspect)
        self.ui.screen_width.editingFinished.connect(self.update_screen_width)
        self.ui.screen_height.editingFinished.connect(self.update_screen_height)
        self.ui.screen_diagonal.editingFinished.connect(self.update_screen_diagonal)
        self.ui.screen_diagonal_inch.editingFinished.connect(self.update_screen_diagonal_inch)

    def update_area(self):
        w = self.ui.screen_width.value()
        h = self.ui.screen_height.value()

        a = (w / 1000) * (h / 1000)
        self.ui.screen_area.setValue(a)

    def update_screen_aspect(self, index):
        # скорректировать в зависимости от элементов в интерфейсе
        horizontal = [16, 4, 16, 1]
        vertical = [10, 3, 9, 1]

        self.aspect_w = horizontal[index]
        self.aspect_h = vertical[index]
        self.aspect_d = sqrt(pow(horizontal[index], 2) + pow(vertical[index], 2))

        self.update_screen_width()

    def update_screen_width(self):
        w = self.ui.screen_width.value()
        h = w / self.aspect_w * self.aspect_h
        d = w / self.aspect_w * self.aspect_d
        i = d / 25.4

        self.ui.screen_height.setValue(int(h))
        self.ui.screen_diagonal.setValue(int(d))
        self.ui.screen_diagonal_inch.setValue(int(i))
        self.update_area()

    def update_screen_height(self):
        h = self.ui.screen_height.value()
        w = h / self.aspect_h * self.aspect_w
        d = h / self.aspect_h * self.aspect_d
        i = d / 25.4

        self.ui.screen_width.setValue(int(w))
        self.ui.screen_diagonal.setValue(int(d))
        self.ui.screen_diagonal_inch.setValue(int(i))
        self.update_area()

    def update_screen_diagonal(self):
        d = self.ui.screen_diagonal.value()
        w = d / self.aspect_d * self.aspect_w
        h = d / self.aspect_d * self.aspect_h
        i = d / 25.4

        self.ui.screen_width.setValue(int(w))
        self.ui.screen_height.setValue(int(h))
        self.ui.screen_diagonal_inch.setValue(int(i))
        self.update_area()

    def update_screen_diagonal_inch(self):
        i = self.ui.screen_diagonal_inch.value()
        d = i * 25.4
        w = d / self.aspect_d * self.aspect_w
        h = d / self.aspect_d * self.aspect_h

        self.ui.screen_width.setValue(int(w))
        self.ui.screen_height.setValue(int(h))
        self.ui.screen_diagonal.setValue(int(d))
        self.update_area()

    @QtCore.pyqtSlot()
    def on_button_send_clicked(self):
        translate = QtCore.QCoreApplication.translate
        values = dict()
        values["width"] = self.ui.screen_width.value()
        values["height"] = self.ui.screen_height.value()
        values["diag"] = self.ui.screen_diagonal.value()
        values["diag_inch"] = self.ui.screen_diagonal_inch.value()
        values["area"] = self.ui.screen_area.value()
        values["aspect"] = self.ui.screen_aspect.currentText()

        template = translate("PartScreenW", """\
<h1>Screen</h1>
<h2>Aspect ratio {aspect}</h2>
<ul>
<li>diagonal: {diag_inch}" ({diag} mm);</li>
<li>width: {width} mm;</li>
<li>height: {height} mm;</li>
<li>area: {area} square meters.</li>
</ul>
<p><br/></p>
""")
        text = template.format(**values)
        self.send_data.emit(text)
