#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
from PyQt5 import QtCore, QtWidgets
from modules import part_sizes_ui


class PartSizesW(QtWidgets.QWidget):
    def __init__(self):
        super(PartSizesW, self).__init__()

        self.ui = part_sizes_ui.Ui_PartSizes()
        self.ui.setupUi(self)

        self.ui.pixel_minutes.editingFinished.connect(self.calculateSizes)
        self.ui.distance.editingFinished.connect(self.calculateSizes)
        self.ui.screen_resolution_h.editingFinished.connect(self.calculateSizes)

    def calculateSizes(self):
        size_minutes = self.ui.pixel_minutes.value()
        size_deg = size_minutes / 60.0
        size_rad = math.radians(size_deg)
        distance = self.ui.distance.value()
        pixel_size = math.tan(size_rad) * distance
        self.ui.pixel_size.setValue(pixel_size)

        resolution_h = self.ui.screen_resolution_h.value()
        self.ui.screen_width.setValue(pixel_size * resolution_h)

        size_rad = math.atan(self.ui.pixel_size.value() / self.ui.distance.value())
        size_deg = math.degrees(size_rad)
        size_minutes = size_deg * 60.0
