# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interfaces/part_sizes.ui'
#
# Created by: PyQt5 UI code generator 5.15.8
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PartSizes(object):
    def setupUi(self, PartSizes):
        PartSizes.setObjectName("PartSizes")
        PartSizes.resize(416, 421)
        self.verticalLayout = QtWidgets.QVBoxLayout(PartSizes)
        self.verticalLayout.setObjectName("verticalLayout")
        self.group_sizes = QtWidgets.QGroupBox(PartSizes)
        self.group_sizes.setObjectName("group_sizes")
        self.formLayout = QtWidgets.QFormLayout(self.group_sizes)
        self.formLayout.setObjectName("formLayout")
        self.label_minutes = QtWidgets.QLabel(self.group_sizes)
        self.label_minutes.setObjectName("label_minutes")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_minutes)
        self.pixel_minutes = QtWidgets.QDoubleSpinBox(self.group_sizes)
        self.pixel_minutes.setReadOnly(False)
        self.pixel_minutes.setSuffix("")
        self.pixel_minutes.setMaximum(9999.99)
        self.pixel_minutes.setObjectName("pixel_minutes")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.pixel_minutes)
        self.label_distance = QtWidgets.QLabel(self.group_sizes)
        self.label_distance.setObjectName("label_distance")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_distance)
        self.distance = QtWidgets.QSpinBox(self.group_sizes)
        self.distance.setMinimum(10)
        self.distance.setMaximum(10000)
        self.distance.setSingleStep(10)
        self.distance.setProperty("value", 3000)
        self.distance.setObjectName("distance")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.distance)
        self.label_resolution_h = QtWidgets.QLabel(self.group_sizes)
        self.label_resolution_h.setObjectName("label_resolution_h")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_resolution_h)
        self.screen_resolution_h = QtWidgets.QSpinBox(self.group_sizes)
        self.screen_resolution_h.setMaximum(10000)
        self.screen_resolution_h.setProperty("value", 1920)
        self.screen_resolution_h.setObjectName("screen_resolution_h")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.screen_resolution_h)
        self.line = QtWidgets.QFrame(self.group_sizes)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.SpanningRole, self.line)
        self.label_size = QtWidgets.QLabel(self.group_sizes)
        self.label_size.setObjectName("label_size")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_size)
        self.pixel_size = QtWidgets.QDoubleSpinBox(self.group_sizes)
        self.pixel_size.setReadOnly(True)
        self.pixel_size.setPrefix("")
        self.pixel_size.setMinimum(0.01)
        self.pixel_size.setMaximum(9999.99)
        self.pixel_size.setProperty("value", 0.01)
        self.pixel_size.setObjectName("pixel_size")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.pixel_size)
        self.label_width = QtWidgets.QLabel(self.group_sizes)
        self.label_width.setObjectName("label_width")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_width)
        self.screen_width = QtWidgets.QDoubleSpinBox(self.group_sizes)
        self.screen_width.setReadOnly(True)
        self.screen_width.setMaximum(9999.99)
        self.screen_width.setObjectName("screen_width")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.screen_width)
        self.verticalLayout.addWidget(self.group_sizes)
        self.label_minutes.setBuddy(self.pixel_minutes)
        self.label_distance.setBuddy(self.distance)
        self.label_resolution_h.setBuddy(self.screen_resolution_h)
        self.label_size.setBuddy(self.pixel_size)
        self.label_width.setBuddy(self.screen_width)

        self.retranslateUi(PartSizes)
        QtCore.QMetaObject.connectSlotsByName(PartSizes)

    def retranslateUi(self, PartSizes):
        _translate = QtCore.QCoreApplication.translate
        PartSizes.setWindowTitle(_translate("PartSizes", "Размеры из минут"))
        self.group_sizes.setTitle(_translate("PartSizes", "Размеры"))
        self.label_minutes.setText(_translate("PartSizes", "Размер в уголовых минутах"))
        self.label_distance.setText(_translate("PartSizes", "Расстояние до экрана"))
        self.distance.setSuffix(_translate("PartSizes", " мм"))
        self.label_resolution_h.setText(_translate("PartSizes", "Разрешение по горизонтали"))
        self.label_size.setText(_translate("PartSizes", "Физический размер пикселя"))
        self.pixel_size.setSuffix(_translate("PartSizes", " мм"))
        self.label_width.setText(_translate("PartSizes", "Ширина экрана"))
        self.screen_width.setSuffix(_translate("PartSizes", " мм"))
