#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtWidgets
from modules import projection_ui
from modules import textual
from modules import roomscreen
from modules import curved
from modules import blend


class ProjectionUI(QtWidgets.QMainWindow):
    def __init__(self):
        super(ProjectionUI, self).__init__()

        self.ui = projection_ui.Ui_ProjectionCAD()
        self.ui.setupUi(self)

        self.ui.action_screen.triggered.connect(self.add_screen)
        self.ui.action_curve.triggered.connect(self.add_curve)
        self.ui.action_blend.triggered.connect(self.add_blend)
        self.ui.action_simple.triggered.connect(self.add_simples)

    def add_screen(self):
        subWindow1 = QtWidgets.QMdiSubWindow()
        subWindow1.setWidget(roomscreen.RoomScreenW())
        subWindow1.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.ui.mdiArea.addSubWindow(subWindow1)
        subWindow1.show()

    def add_curve(self):
        subWindow1 = QtWidgets.QMdiSubWindow()
        subWindow1.setWidget(curved.CylindricalScreenW())
        subWindow1.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.ui.mdiArea.addSubWindow(subWindow1)
        subWindow1.show()

    def add_blend(self):
        subWindow1 = QtWidgets.QMdiSubWindow()
        subWindow1.setWidget(blend.ProjectorBlend())
        subWindow1.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.ui.mdiArea.addSubWindow(subWindow1)
        subWindow1.show()

    def add_simples(self):
        subWindow1 = QtWidgets.QMdiSubWindow()
        subWindow1.setWidget(textual.Textual())
        subWindow1.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.ui.mdiArea.addSubWindow(subWindow1)
        subWindow1.show()

    def closeEvent(self, e):
        translate = QtCore.QCoreApplication.translate

        result = QtWidgets.QMessageBox.question(self, translate("ProjectionCAD", "Question:"),
                                                translate("ProjectionCAD",
                                                          "Do you really want to exit?"),
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            e.accept()
        else:
            e.ignore()
