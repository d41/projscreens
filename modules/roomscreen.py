#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import os.path
import math
from collections import namedtuple

from PyQt5 import QtCore, QtGui, QtWidgets

from modules import roomscreen_ui
from modules import calculate
from modules.schemes import Scheme

logger = logging.getLogger(__name__)

RoomBlock = namedtuple("RoomBlock", ("width", "length", "height", "low_end"))
ScreenBlock = namedtuple("ScreenBlock", ("width", "height", "diagonal", "diagonal_inch", "low_end"))
AngularBlock = namedtuple("AngularBlock", ("near", "far", "optimal"))


class RoomScreenW(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.ui = roomscreen_ui.Ui_RoomScreen()
        self.ui.setupUi(self)
        self.ui.gv_side.setScene(SideScheme())
        self.ui.gv_top.setScene(TopScheme())

        self._multiplier = 6
        self.get_multiplier(1)
        self.aspect_w = 0
        self.aspect_h = 0
        self.aspect_d = 0
        self.update_screen_aspect(self.ui.screen_aspect.currentIndex())

        self.ui.room_height.editingFinished.connect(self.update_limit_height)
        self.ui.room_screen.editingFinished.connect(self.update_limit_height)
        self.ui.room_length.editingFinished.connect(self.update_limit_diagonal)
        self.ui.viewer_first.editingFinished.connect(self.update_limit_max)
        self.ui.viewer_last.editingFinished.connect(self.update_limit_min)
        self.ui.system_k.currentIndexChanged.connect(self.get_multiplier)

        self.ui.screen_aspect.currentIndexChanged.connect(self.update_screen_aspect)
        self.ui.screen_width.editingFinished.connect(self.update_screen_width)
        self.ui.screen_height.editingFinished.connect(self.update_screen_height)
        self.ui.screen_diagonal.editingFinished.connect(self.update_screen_diagonal)
        self.ui.screen_diagonal_inch.editingFinished.connect(self.update_screen_diagonal_inch)

        self.update_limit_height()
        self.update_limit_diagonal()
        self.update_limit_max()
        self.update_limit_min()

    def get_multiplier(self, index):
        self._multiplier = (8, 6, 4)[index]
        self.update_limit_min()

    def update_limit_height(self):
        self.ui.limit_height.setValue(int(self.ui.room_height.value() - self.ui.room_screen.value()))

    def update_limit_diagonal(self):
        self.ui.limit_diagonal.setValue(int(self.ui.room_length.value() / 2))

    def update_limit_max(self):
        self.ui.limit_max.setValue(int(self.ui.viewer_first.value() / 2))

    def update_limit_min(self):
        self.ui.limit_min.setValue(int(self.ui.viewer_last.value() / self._multiplier))

    def update_area(self):
        w = self.ui.screen_width.value()
        h = self.ui.screen_height.value()

        a = (w / 1000) * (h / 1000)
        self.ui.screen_area.setValue(int(a))

    def update_screen_aspect(self, index):
        # скорректировать в зависимости от элементов в интерфейсе
        horizontal = [16, 4, 16, 1]
        vertical = [10, 3, 9, 1]

        self.aspect_w = horizontal[index]
        self.aspect_h = vertical[index]
        self.aspect_d = math.sqrt(self.aspect_w ** 2 + self.aspect_h ** 2)

        self.update_screen_height()

    def update_screen_info(self, item):
        w = item * self.aspect_w
        h = item * self.aspect_h
        d = item * self.aspect_d
        i = calculate.mm_to_inch(d)
        return w, h, d, i

    def update_screen_height(self):
        item = self.ui.screen_height.value() / self.aspect_h
        w, h, d, i = self.update_screen_info(item)

        self.ui.screen_width.setValue(int(w))
        self.ui.screen_diagonal.setValue(int(d))
        self.ui.screen_diagonal_inch.setValue(int(i))
        self.update_area()

    def update_screen_width(self):
        item = self.ui.screen_width.value() / self.aspect_w
        w, h, d, i = self.update_screen_info(item)

        self.ui.screen_height.setValue(int(h))
        self.ui.screen_diagonal.setValue(int(d))
        self.ui.screen_diagonal_inch.setValue(int(i))
        self.update_area()

    def update_screen_diagonal(self):
        item = self.ui.screen_diagonal.value() / self.aspect_d
        w, h, d, i = self.update_screen_info(item)

        self.ui.screen_width.setValue(int(w))
        self.ui.screen_height.setValue(int(h))
        self.ui.screen_diagonal_inch.setValue(int(i))
        self.update_area()

    def update_screen_diagonal_inch(self):
        item = calculate.inch_to_mm(self.ui.screen_diagonal_inch.value()) / self.aspect_d
        w, h, d, i = self.update_screen_info(item)

        self.ui.screen_width.setValue(int(w))
        self.ui.screen_height.setValue(int(h))
        self.ui.screen_diagonal.setValue(int(d))
        self.update_area()

    @QtCore.pyqtSlot()
    def on_draw_room_clicked(self):
        print("Draw room:")
        room = RoomBlock(
            width=self.ui.room_width.value(),
            length=self.ui.room_length.value(),
            height=self.ui.room_height.value(),
            low_end=self.ui.room_screen.value(),
        )
        angular = AngularBlock(
            near=self.ui.viewer_first.value(),
            far=self.ui.viewer_last.value(),
            optimal=self._multiplier * self.ui.screen_height.value(),
        )
        self.ui.gv_side.scene().update_room(room, angular, scale=0.05)
        self.ui.gv_top.scene().update_room(room, angular, scale=0.05)

    @QtCore.pyqtSlot()
    def on_draw_screen_clicked(self):
        print("Draw screen:")
        screen = ScreenBlock(
            width=self.ui.screen_width.value(),
            height=self.ui.screen_height.value(),
            diagonal=self.ui.screen_diagonal.value(),
            diagonal_inch=self.ui.screen_diagonal_inch.value(),
            low_end=self.ui.room_screen.value(),
        )
        angular = AngularBlock(
            near=self.ui.viewer_first.value(),
            far=self.ui.viewer_last.value(),
            optimal=self._multiplier * self.ui.screen_height.value(),
        )
        self.ui.gv_side.scene().update_screen(screen, angular, scale=0.05)
        self.ui.gv_top.scene().update_screen(screen, angular, scale=0.05)

    def prepare_text(self):
        room = RoomBlock(
            width=self.ui.room_width.value(),
            length=self.ui.room_length.value(),
            height=self.ui.room_height.value(),
            low_end=self.ui.room_screen.value(),
        )
        screen = ScreenBlock(
            width=self.ui.screen_width.value(),
            height=self.ui.screen_height.value(),
            diagonal=self.ui.screen_diagonal.value(),
            diagonal_inch=self.ui.screen_diagonal_inch.value(),
            low_end=self.ui.room_screen.value(),
        )
        angular = AngularBlock(
            near=self.ui.viewer_first.value(),
            far=self.ui.viewer_last.value(),
            optimal=self._multiplier * self.ui.screen_height.value(),
        )
        usage_factor = self._multiplier;
        aspect_ratio = self.ui.screen_aspect.currentText()
        area = self.ui.screen_area.value()

        limit_height = self.ui.limit_height.value()
        limit_distance = usage_factor * screen.height
        limit_minimal = self.ui.limit_min.value()
        limit_maximal = self.ui.limit_max.value()
        limit_diagonal = self.ui.limit_diagonal.value()
        comment = "None." if limit_minimal < limit_maximal else "Additional screens required."

        text = ""
        with open(os.path.join("templates", "room_screen.html"), "rt") as file:
            text = file.read()
            text = text.format(**vars())

        return text

    def get_document(self):
        editor = QtWidgets.QTextEdit()
        document = QtGui.QTextDocument()
        editor.setDocument(document)

        side_image = self.ui.gv_side.scene().get_image()
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/rs_side.png"), side_image)

        top_image = self.ui.gv_top.scene().get_image()
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/rs_top.png"), top_image)

        text = self.prepare_text()
        editor.append(text)
        document.end()

        return document

    @QtCore.pyqtSlot()
    def on_copy_data_clicked(self):
        print("Copy to clipboard:")
        document = self.get_document()

        data = QtCore.QMimeData()
        data.setHtml(document.toHtml())
        data.setUrls([QtCore.QUrl("dynamic:/images/rs_side.png")])  # , QtCore.QUrl("dynamic:/images/rs_top.png")])
        data.setImageData(
            document.resource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/rs_side.png")))

        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setMimeData(data, QtGui.QClipboard.Clipboard)

    @QtCore.pyqtSlot()
    def on_print_data_clicked(self):
        print("Print dialog:")
        from PyQt5 import QtPrintSupport

        document = self.get_document()

        printer = QtPrintSupport.QPrinter()
        print_dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        print_dialog.paintRequested.connect(document.print)
        if print_dialog.exec() != print_dialog.Accepted:
            return

        document.print(printer)


# Функции и объекты отрисовки
class SideScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.room = None
        self.room_annotations = None
        self.room_viewers = None

        self.screen = None
        self.screen_annotations = None

        self.pen_projection.setWidth(2)
        self.pen_viewers = QtGui.QPen(QtCore.Qt.blue)
        self.pen_viewers.setStyle(QtCore.Qt.DashLine)
        self.pen_sub_optimal = QtGui.QPen(QtCore.Qt.darkGreen)
        self.pen_sub_optimal.setStyle(QtCore.Qt.DashLine)

        self.reset_room()
        self.reset_screen()

    def reset_room(self):
        self.room = None
        self.room_annotations = []
        self.room_viewers = []

    def clear_room(self):
        for item in self.room_viewers:
            self.removeItem(item)
            del item
        for item in self.room_annotations:
            self.removeItem(item)
            del item
        if self.room is not None:
            self.removeItem(self.room)
            del self.room

    def reset_screen(self):
        self.screen = None
        self.screen_annotations = []

    def clear_screen(self):
        for item in self.screen_annotations:
            self.removeItem(item)
            del item
        if self.screen is not None:
            self.removeItem(self.screen)
            del self.screen

    def update_room(self, room, angular, scale=1.0):
        self.clear_room()
        self.reset_room()

        # draw room
        length = room.length * scale
        height = -room.height * scale
        self.room = self.addRect(0, 0, length, height, pen=self.pen_surface)
        d = self.drawHorizontalDimension(0, height, length, height, height - 15, "{:d}".format(room.length))
        self.room_annotations.append(d)
        d = self.drawVerticalDimension(0, height, 0, 0, -35, "{:d}".format(room.height))
        self.room_annotations.append(d)

        # low end
        level = -room.low_end * scale
        d = self.addLine(0, level, length, level, pen=self.pen_illusion)
        self.room_annotations.append(d)
        d = self.drawVerticalDimension(0, 0, 0, level, -15, "{:d}".format(room.low_end))
        self.room_annotations.append(d)

        # first  viewer, last viewer
        a, b = angular.near * scale, angular.far * scale
        d = self.addLine(a, 0, a, height, pen=self.pen_viewers)
        self.room_viewers.append(d)
        d = self.addLine(b, 0, b, height, pen=self.pen_viewers)
        self.room_viewers.append(d)

    def update_screen(self, screen, angular, scale=1.0):
        self.clear_screen()
        self.reset_screen()

        # draw screen
        top = (-screen.low_end - screen.height) * scale
        bottom = -screen.low_end * scale
        height = screen.height * scale
        self.screen = self.addRect(-2, top, 4, height, pen=self.pen_projection)
        d = self.drawVerticalDimension(0, top, 0, top + height, -15, "{:d}".format(screen.height))
        self.screen_annotations.append(d)

        # viewer
        optimal = angular.optimal * scale
        d = self.addLine(optimal, 0, optimal, -height, pen=self.pen_sub_optimal)
        self.screen_annotations.append(d)
        suboptimal = 2 * screen.height * scale
        d = self.addLine(suboptimal, 0, suboptimal, -height, pen=self.pen_sub_optimal)
        self.screen_annotations.append(d)

        # first  viewer, last viewer
        a, b = angular.near * scale, angular.far * scale
        d = self.addLine(a, bottom, 0, top, pen=self.pen_illusion)
        self.screen_annotations.append(d)
        d = self.addLine(b, bottom, 0, top, pen=self.pen_illusion)
        self.screen_annotations.append(d)

        d = self.drawAngularDimension(a, bottom, 0, bottom, 0, top, 50, None)
        self.screen_annotations.append(d)
        d = self.drawAngularDimension(b, bottom, 0, bottom, 0, top, 50, None)
        self.screen_annotations.append(d)


class TopScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.room = None
        self.room_annotations = None
        self.room_viewers = None

        self.screen = None
        self.screen_annotations = None

        self.pen_projection.setWidth(2)
        self.pen_viewers = QtGui.QPen(QtCore.Qt.blue)
        self.pen_viewers.setStyle(QtCore.Qt.DashLine)
        self.pen_sub_optimal = QtGui.QPen(QtCore.Qt.darkGreen)
        self.pen_sub_optimal.setStyle(QtCore.Qt.DashLine)

        self.reset_room()
        self.reset_screen()

    def reset_room(self):
        self.room = None
        self.room_annotations = []
        self.room_viewers = []

    def clear_room(self):
        for item in self.room_viewers:
            self.removeItem(item)
            del item
        for item in self.room_annotations:
            self.removeItem(item)
            del item
        if self.room is not None:
            self.removeItem(self.room)
            del self.room

    def reset_screen(self):
        self.screen = None
        self.screen_annotations = []

    def clear_screen(self):
        for item in self.screen_annotations:
            self.removeItem(item)
            del item
        if self.screen is not None:
            self.removeItem(self.screen)
            del self.screen

    def update_room(self, room, angular, scale=1.0):
        self.clear_room()
        self.reset_room()

        # draw room
        width = room.width * scale
        length = room.length * scale
        half_width = width / 2
        self.room = self.addRect(0, -half_width, length, width, pen=self.pen_surface)
        d = self.drawHorizontalDimension(0, -half_width, length, -half_width, -half_width - 15,
                                         "{:d}".format(room.length))
        self.room_annotations.append(d)
        d = self.drawVerticalDimension(0, -half_width, 0, half_width, -35, "{:d}".format(room.width))
        self.room_annotations.append(d)
        d = self.addLine(-10, 0, length + 10, 0, pen=self.pen_symmetry)
        self.room_annotations.append(d)

        # first  viewer, last viewer
        a, b = angular.near * scale, angular.far * scale
        d = self.addLine(a, -half_width, a, half_width, pen=self.pen_viewers)
        self.room_viewers.append(d)
        d = self.addLine(b, -half_width, b, half_width, pen=self.pen_viewers)
        self.room_viewers.append(d)

        d = self.addLine(0, 0, a, half_width, pen=self.pen_illusion)
        self.room_viewers.append(d)
        d = self.addLine(0, 0, b, half_width, pen=self.pen_illusion)
        self.room_viewers.append(d)
        d = self.drawAngularDimension(0, 0, a, -half_width, a, half_width, 50, None)
        self.room_viewers.append(d)
        d = self.drawAngularDimension(0, 0, b, -half_width, b, half_width, 100, None)
        self.room_viewers.append(d)

    def update_screen(self, screen, angular, scale=1.0):
        self.clear_screen()
        self.reset_screen()

        # draw screen
        width = screen.width * scale
        top = -width / 2
        self.screen = self.addRect(-2, top, 4, width, pen=self.pen_projection)
        d = self.drawVerticalDimension(0, top, 0, width / 2, -15, "{:d}".format(screen.width))
        self.screen_annotations.append(d)

        # viewer
        optimal = angular.optimal * scale
        d = self.addLine(optimal, top, optimal, width / 2, pen=self.pen_sub_optimal)
        self.screen_annotations.append(d)
        suboptimal = 2 * screen.height * scale
        d = self.addLine(suboptimal, top, suboptimal, width / 2, pen=self.pen_sub_optimal)
        self.screen_annotations.append(d)
