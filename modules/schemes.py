# -*- coding: utf-8 -*-
import math
from PyQt5 import QtWidgets, QtGui, QtCore
from modules import calculate

__author__ = "d41"


class LinearDimensionItem(QtWidgets.QGraphicsItem):
    def __init__(self, x1, y1, x2, y2, label="", arrows=3, parent=None):
        """
        :param arrows: 0 - no arrow, 1 - at (x1,y1), 2 - at (x2,y2), 3 = 1+2
        """
        super().__init__(parent)

        self.pen_dimensions = QtGui.QPen(QtCore.Qt.black)
        self.pen_dimensions.setWidth(1)
        self.font = QtGui.QFont("Serif", 10)

        self.x1, self.x2 = int(x1), int(x2)
        self.y1, self.y2 = int(y1), int(y2)
        self.label = label
        self.font_height = QtGui.QFontMetricsF(self.font).height()
        self.angle = calculate.line_angle(self.x1, self.y1, self.x2, self.y2)

        self.draw_arrows = arrows
        self.arrow_x = 10
        self.arrow_y = 3

        self._bounding_box = self.get_boundaries()

    def boundingRect(self):
        # recalculate using actual data and label
        left, top, width, height = self._bounding_box

        return QtCore.QRectF(left, top, width, height)

    def get_boundaries(self):
        dx = self.x2 - self.x1
        dy = self.y2 - self.y1

        if dx == 0:
            left = self.x1 - self.font_height
            top = min(self.y1, self.y2)
            width = self.font_height + self.arrow_y
            height = math.fabs(dy)

        elif dy == 0:
            left = min(self.x1, self.x2)
            top = self.y1 - self.font_height
            width = math.fabs(dx)
            height = self.font_height + self.arrow_y

        else:
            segment, _ = divmod(self.angle, 90)
            angle = self.angle if segment in [0, 3] else self.angle + 180
            x3, y3 = calculate.line_by_angle(self.x1, self.y1, angle - 90, self.font_height)
            x4, y4 = calculate.line_by_angle(self.x2, self.y2, angle - 90, self.font_height)
            points = [(self.x1, self.y1), (self.x2, self.y2), (x4, y4), (x3, y3)]

            left = min(points, key=lambda x: x[0])[0]
            top = min(points, key=lambda x: x[1])[1]
            right = max(points, key=lambda x: x[0])[0]
            bottom = max(points, key=lambda x: x[1])[1]

            width = right - left
            height = bottom - top

        return left, top, width, height

    def paint(self, painter, option, widget):
        painter.setPen(self.pen_dimensions)
        painter.setFont(self.font)

        d_len = int(calculate.line_length(self.x1, self.y1, self.x2, self.y2))

        # draw main line
        painter.drawLine(self.x1, self.y1, self.x2, self.y2)

        # draw arrows
        painter.save()
        painter.translate(self.x1, self.y1)
        painter.rotate(self.angle)
        painter.setBrush(QtGui.QBrush(QtCore.Qt.black))

        if self.draw_arrows & 1 > 0:
            painter.drawPolygon(QtGui.QPolygon([
                QtCore.QPoint(0, 0),
                QtCore.QPoint(self.arrow_x, self.arrow_y),
                QtCore.QPoint(self.arrow_x, -self.arrow_y)],
            ))
        if self.draw_arrows & 2 > 0:
            painter.drawPolygon(QtGui.QPolygon([
                QtCore.QPoint(d_len, 0),
                QtCore.QPoint(d_len - self.arrow_x, self.arrow_y),
                QtCore.QPoint(d_len - self.arrow_x, -self.arrow_y)],
            ))
        painter.restore()

        # draw label
        painter.save()

        # rotate label to drafting standard
        segment, point = divmod(self.angle, 90)
        if segment in [0, 3]:
            painter.translate(self.x1, self.y1)
            painter.rotate(self.angle)
        else:
            painter.translate(self.x2, self.y2)
            painter.rotate(180 + self.angle)

        painter.drawText(QtCore.QRectF(0, -self.font_height, d_len, self.font_height),
                         QtCore.Qt.AlignCenter, self.label)

        painter.restore()


class AngularDimensionItem(QtWidgets.QGraphicsItem):
    def __init__(self, x0, y0, x1, y1, x2, y2, distance, label=None, parent=None):
        super().__init__(parent)

        self.pen_dimensions = QtGui.QPen(QtCore.Qt.black)
        self.pen_dimensions.setWidth(1)
        self.font = QtGui.QFont("Serif", 10)
        self.font_height = QtGui.QFontMetricsF(self.font).height()
        fm = QtGui.QFontMetrics(self.font)

        self.label = label
        self.distance = distance
        self.arrow_x = 10
        self.arrow_y = 3

        self.angle1 = calculate.line_angle(x0, y0, x1, y1)
        self.angle2 = calculate.line_angle(x0, y0, x2, y2)
        if self.label is None:
            self.label = "{:0.01f}".format((self.angle2 - self.angle1 + 360) % 360)
        self.label_width = fm.width(self.label)  # horizontalAdvance

        self.x0, self.y0 = int(x0), int(y0)
        self.x1, self.y1 = calculate.line_by_angle(x0, y0, self.angle1, distance)
        self.x2, self.y2 = calculate.line_by_angle(x0, y0, self.angle2, distance)

        self.x1, self.y1 = int(self.x1), int(self.y1)
        self.x2, self.y2 = int(self.x2), int(self.y2)

        self._bounding_box = self.get_boundaries()

    def boundingRect(self):
        # recalculate using actual data and label
        left, top, width, height = self._bounding_box

        return QtCore.QRectF(left, top, width, height)

    def get_boundaries(self):
        # main points
        xs = [self.x0, self.x1, self.x2]
        ys = [self.y0, self.y1, self.y2]

        diff_a = self.angle2 - self.angle1
        split_a = diff_a / 2 + self.angle1 if diff_a >= 0 else (diff_a + 360) / 2 + self.angle1
        xc, yc = calculate.line_by_angle(self.x0, self.y0, split_a, self.distance)
        xc, yc = int(xc), int(yc)

        # farthest point of label
        label_x, label_y = xc, yc
        if split_a < 90:
            label_x += self.label_width
            label_y += self.font_height
        elif split_a < 180:
            label_x -= self.label_width
            label_y += self.font_height
        elif split_a < 270:
            label_x -= self.label_width
            label_y -= self.font_height
        else:
            label_x += self.label_width
            label_y -= self.font_height
        xs.append(label_x)
        ys.append(label_y)

        # arc points
        if self.angle1 < 0 < self.angle2:
            xs.append(self.x0 + self.distance)
        if self.angle1 < 90 < self.angle2:
            ys.append(self.y0 + self.distance)
        if self.angle1 < 180 < self.angle2:
            xs.append(self.x0 - self.distance)
        if self.angle1 < 270 < self.angle2:
            ys.append(self.y0 - self.distance)

        # getting rectangle
        left = min(xs)
        right = max(xs)
        top = min(ys)
        bottom = max(ys)
        width = right - left
        height = bottom - top

        return left, top, width, height

    def paint(self, painter, option, widget):
        painter.setPen(self.pen_dimensions)
        painter.setFont(self.font)

        diameter = int(self.distance * 2)
        radius = int(self.distance)
        diff_a = (self.angle2 - self.angle1 + 360) % 360
        start_a, span_a = self.angle1 * 16, int(diff_a * 16)

        # draw main line
        painter.save()
        painter.translate(self.x0, self.y0)
        painter.rotate(self.angle1)

        painter.drawArc(-radius, -radius, diameter, diameter, 0, -span_a)

        painter.restore()

        # draw arrows
        painter.save()
        painter.translate(self.x1, self.y1)
        painter.rotate(self.angle1 + 90)

        painter.setBrush(QtGui.QBrush(QtCore.Qt.black))
        painter.drawPolygon(QtGui.QPolygon([
            QtCore.QPoint(0, 0),
            QtCore.QPoint(self.arrow_x, self.arrow_y),
            QtCore.QPoint(self.arrow_x, -self.arrow_y)],
        ))
        painter.restore()

        painter.save()
        painter.translate(self.x2, self.y2)
        painter.rotate(self.angle2 - 90)

        painter.setBrush(QtGui.QBrush(QtCore.Qt.black))
        painter.drawPolygon(QtGui.QPolygon([
            QtCore.QPoint(0, 0),
            QtCore.QPoint(self.arrow_x, self.arrow_y),
            QtCore.QPoint(self.arrow_x, -self.arrow_y)],
        ))
        painter.restore()

        # draw label
        diff_a = self.angle2 - self.angle1
        split_a = diff_a / 2 + self.angle1 if diff_a >= 0 else (diff_a + 360) / 2 + self.angle1
        xc, yc = calculate.line_by_angle(self.x0, self.y0, split_a, self.distance)
        xc, yc = int(xc), int(yc)

        painter.save()
        painter.translate(xc, yc)
        if split_a < 90:
            painter.translate(0, self.font_height)
        elif split_a < 180:
            painter.translate(-self.label_width, self.font_height)
        elif split_a < 270:
            painter.translate(-self.label_width, 0)
        else:
            painter.translate(0, 0)

        painter.drawText(0, 0, self.label)

        painter.restore()


class Scheme(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.pen_surface = QtGui.QPen(QtCore.Qt.black)
        self.pen_surface.setWidth(3)

        self.pen_projection = QtGui.QPen(QtCore.Qt.red)
        self.pen_projection.setWidth(1)

        self.pen_projector = QtGui.QPen(QtCore.Qt.black)
        self.pen_projector.setWidth(2)

        self.pen_symmetry = QtGui.QPen(QtCore.Qt.black)
        self.pen_symmetry.setWidth(1)
        self.pen_symmetry.setStyle(QtCore.Qt.DashDotLine)

        self.pen_illusion = QtGui.QPen(QtCore.Qt.black)
        self.pen_illusion.setWidth(1)
        self.pen_illusion.setStyle(QtCore.Qt.DashLine)

    def drawHorizontalDimension(self, x1, y1, x2, y2, y_line, label):
        tail = 5
        group = QtWidgets.QGraphicsItemGroup()
        dimension = LinearDimensionItem(x1, y_line, x2, y_line, label)
        group.addToGroup(dimension)

        line = self.addLine(x1, y1, x1, y_line + math.copysign(tail, y_line - y1))
        group.addToGroup(line)

        line = self.addLine(x2, y2, x2, y_line + math.copysign(tail, y_line - y2))
        group.addToGroup(line)
        self.addItem(group)

        return group

    def drawVerticalDimension(self, x1, y1, x2, y2, x_line, label):
        tail = 5
        group = QtWidgets.QGraphicsItemGroup()
        dimension = LinearDimensionItem(x_line, y1, x_line, y2, label)
        group.addToGroup(dimension)

        line = self.addLine(x1, y1, x_line + math.copysign(tail, x_line - x1), y1)
        group.addToGroup(line)

        line = self.addLine(x2, y2, x_line + math.copysign(tail, x_line - x2), y2)
        group.addToGroup(line)
        self.addItem(group)

        return group

    def drawRadiusDimension(self, x, y, radius, angle, length, label):
        x_on_arc, y_on_arc = calculate.line_by_angle(x, y, angle, radius)
        x_tail, y_tail = calculate.line_by_angle(x_on_arc, y_on_arc, (180 + angle) % 360, length)
        dimension = LinearDimensionItem(x_tail, y_tail, x_on_arc, y_on_arc, label, arrows=2)
        self.addItem(dimension)

        return dimension

    def drawAngularDimension(self, x0, y0, x1, y1, x2, y2, distance, label):
        tail = 5

        group = QtWidgets.QGraphicsItemGroup()
        dimension = AngularDimensionItem(x0, y0, x1, y1, x2, y2, distance, label)
        group.addToGroup(dimension)

        x_tail, y_tail = calculate.line_by_angle(x0, y0, dimension.angle1, distance + tail)
        line = self.addLine(x0, y0, x_tail, y_tail)
        group.addToGroup(line)
        x_tail, y_tail = calculate.line_by_angle(x0, y0, dimension.angle2, distance + tail)
        line = self.addLine(x0, y0, x_tail, y_tail)
        group.addToGroup(line)
        self.addItem(group)

        return group

    @QtCore.pyqtSlot(QtGui.QContextMenuEvent)
    def contextMenuEvent(self, event):
        copy_action = QtWidgets.QAction("Copy")
        copy_action.setStatusTip("Copy scheme to clipboard")
        copy_action.triggered.connect(self.copy_image_to_clipboard)

        self.contextmenu = QtWidgets.QMenu("Context")
        self.contextmenu.addAction(copy_action)
        self.contextmenu.exec_(event.screenPos())

    def get_image(self):
        image = QtGui.QImage(self.width(), self.height(), QtGui.QImage.Format_ARGB32)
        image.fill(QtGui.QColor(255, 255, 255, alpha=0))

        painter = QtGui.QPainter(image)
        self.render(painter)
        painter.end()
        return image

    def copy_image_to_clipboard(self):
        image = self.get_image()
        data = QtCore.QMimeData()
        data.setImageData(image)

        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setMimeData(data, QtGui.QClipboard.Clipboard)
        # clipboard.setImage(image)


    def testDimensions(self):
        # TODO: вынести из класса куда нибудь
        divisor = 5  # 12
        self.annotations = []

        length = 120
        x1, y1 = 40, 30
        self.addRect(0, 0, x1, y1, pen=QtGui.QPen(QtCore.Qt.red))
        self.addRect(x1, y1, length, length, pen=QtGui.QPen(QtCore.Qt.blue))

        points = []
        for i in range(divisor):
            angle = (360 / divisor) * i
            x2, y2 = calculate.line_by_angle(x1, y1, angle, length)
            points.append((x2, y2))
            d_item = LinearDimensionItem(x1, y1, x2, y2, label="{:.02f}".format(angle))
            self.addItem(d_item)
            self.annotations.append(d_item)

        for p1, p2 in zip(points, points[1:] + points[:1]):
            x2, y2 = p1
            x3, y3 = p2
            d_item = self.drawAngularDimension(x1, y1, x2, y2, x3, y3, 80, None)
            self.annotations.append(d_item)
            self.addRect(d_item.boundingRect(), pen=QtGui.QPen(QtCore.Qt.red))
        return
        length = 120
        x1, y1 = 250, 150
        for i in range(1, divisor):
            angle = (360 / divisor) * i
            x2, y2 = x1 + length, y1
            x3, y3 = calculate.line_by_angle(x1, y1, angle, length)
            d_item = self.drawAngularDimension(x1, y1, x2, y2, x3, y3, i * 20, None)
            self.annotations.append(d_item)
            self.addRect(d_item.boundingRect(), pen=QtGui.QPen(QtCore.Qt.red))
        


if '__main__'==__name__:
    import sys
    import calculate

    app = QtWidgets.QApplication(sys.argv)
    win = QtWidgets.QMainWindow()
    s = Scheme()
    sch = QtWidgets.QGraphicsView(s)
    win.setCentralWidget(sch)
    s.testDimensions()
    win.show()
    sys.exit(app.exec_())
