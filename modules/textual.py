#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from PyQt5 import QtCore, QtGui, QtWidgets
from modules import textual_ui

logger = logging.getLogger(__name__)

class Textual(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.ui = textual_ui.Ui_Textual()
        self.ui.setupUi(self)

        # self.ui.action_new.triggered.connect(self.clearNotes)
        # self.ui.action_save.triggered.connect(self.saveToFile)

        # так как мы наследуемся
        self.ui.textEdit.canInsertFromMimeData = self.textCanInsertFromMimeData
        self.ui.textEdit.insertFromMimeData = self.textInsertFromMimeData
        self.images = 0

        # потом сделать по аналогии остальные страницы калькулятора
        self.ui.page_room.send_data.connect(self.addon)
        self.ui.page_screen.send_data.connect(self.addon)
        self.ui.page_brightness.send_data.connect(self.addon)
        self.ui.page_distance.send_data.connect(self.addon)
        self.data_changed = False

        logger.info("Textual window created")

    def closeEvent(self, e):
        translate = QtCore.QCoreApplication.translate

        if self.data_changed:
            result = QtWidgets.QMessageBox.question(self, translate("ProjectionCAD", "Question:"),
                                                    translate("ProjectionCAD",
                                                              "Unsaved data exists, do you really want to close window?"),
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)
            if result == QtWidgets.QMessageBox.Yes:
                e.accept()
            else:
                e.ignore()
        else:
            e.accept()

    def saveToFile(self):
        translate = QtCore.QCoreApplication.translate

        ext = ".html"
        options = QtWidgets.QFileDialog.Options()
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, translate("ProjectionCAD", "Save file"), "",
                                                            "HTML Files (*{})".format(ext), options=options)
        if fileName:
            if not fileName.endswith(ext):
                fileName = fileName + ext
            writeFile = open(fileName, 'w', encoding='utf-8')
            writeFile.write(self.ui.textEdit.toHtml())
            writeFile.close()
            self.ui.statusbar.showMessage('Saved to %s' % fileName)
            self.data_changed = False

    def clearNotes(self, e):
        translate = QtCore.QCoreApplication.translate

        if self.data_changed:
            result = QtWidgets.QMessageBox.question(self, translate("ProjectionCAD", "Question:"),
                                                    translate("ProjectionCAD",
                                                              "Unsaved data exists, do you really want to clear notes?"),
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)
            if result == QtWidgets.QMessageBox.Yes:
                self.ui.textEdit.clear()
        else:
            self.ui.textEdit.clear()

    @QtCore.pyqtSlot(str)
    def addon(self, text):
        self.ui.textEdit.append(text)
        self.ui.textEdit.moveCursor(QtGui.QTextCursor.End, QtGui.QTextCursor.MoveAnchor)
        self.data_changed = True

    def textCanInsertFromMimeData(self, source):
        if source.hasImage():
            return True
        else:
            return QtWidgets.QTextEdit.canInsertFromMimeData(self.ui.textEdit, source)

    def textInsertFromMimeData(self, source):
        # TODO: подумать обо всей этой фигне
        logger.debug("Paste to textEdit")
        print("has formats:", source.formats())

        if source.hasHtml() or source.hasText():
            print("inserting text")
            QtWidgets.QTextEdit.insertFromMimeData(self.ui.textEdit, source)

            if source.hasUrls():
                print("has urls:", source.urls())

            if source.hasImage():
                print("inserting embedded image")
                image = QtGui.QImage(source.imageData())
                url = source.urls()[0] if source.hasUrls() else QtCore.QUrl("dynamic:/images/random")
                print("data img:", source.data('application/x-qt-image'))

                cursor = self.ui.textEdit.textCursor()
                document = self.ui.textEdit.document()
                document.addResource(QtGui.QTextDocument.ImageResource, url, image)
                # cursor.insertImage("image")

        elif source.hasImage():
            print("inserting single image")
            image = QtGui.QImage(source.imageData())

            cursor = self.ui.textEdit.textCursor()
            document = self.ui.textEdit.document()

            self.images += 1
            url = "dynamic://images/pasted_{:d}".format(self.images)
            document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl(url), image)
            cursor.insertImage(url)
