#!/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys
import logging
from PyQt5 import QtWidgets, QtCore
from modules import projection


def main():
    app = QtWidgets.QApplication(sys.argv)

    locales = {}
    locales["ru_RU"] = "projection_ru"

    locale = QtCore.QLocale.system().name()
    if locale in locales:
        translator = QtCore.QTranslator()
        translator.load("translation/" + locales[locale])
        app.installTranslator(translator)

    win = projection.ProjectionUI()
    win.show()
    sys.exit(app.exec_())


if "__main__" == __name__:
    logging.basicConfig(filename='projscreens.log', level=logging.INFO)
    logging.info("Application started")
    main()
