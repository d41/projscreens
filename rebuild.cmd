rem compile designer's UI file

set name=blend
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=curved
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_angles
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_brightness
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_contrast
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_distance
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_room
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_screen
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=part_sizes
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=projection
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=roomscreen
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui

set name=textual
python -m PyQt5.uic.pyuic -o modules/%name%_ui.py interfaces/%name%.ui
