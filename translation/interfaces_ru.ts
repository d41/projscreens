<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>PartAngles</name>
    <message>
        <location filename="../interfaces/part_angles.ui" line="14"/>
        <location filename="../interfaces/part_angles.ui" line="249"/>
        <source>Угловые размеры</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="20"/>
        <source>Абсолютные размеры</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="26"/>
        <location filename="../interfaces/part_angles.ui" line="383"/>
        <source>Ширина изображения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="36"/>
        <location filename="../interfaces/part_angles.ui" line="59"/>
        <location filename="../interfaces/part_angles.ui" line="226"/>
        <location filename="../interfaces/part_angles.ui" line="236"/>
        <location filename="../interfaces/part_angles.ui" line="419"/>
        <source> мм</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="49"/>
        <location filename="../interfaces/part_angles.ui" line="373"/>
        <source>Высота изображения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="72"/>
        <source>Разрешение, горизонталь</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="89"/>
        <source>Разрешение, вертикаль</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="106"/>
        <source>Разрешение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="117"/>
        <source>1920x1200 (WUXGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="122"/>
        <source>1680x1050 (WSXGA+)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="127"/>
        <source>1280x800 (WXGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="132"/>
        <source>1920x1080 (FHD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="137"/>
        <source>1600x900 (11)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="142"/>
        <source>1366x768 (NT)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="147"/>
        <source>1280x720 (HD/WXGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="152"/>
        <source>854x480 (FWVGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="157"/>
        <source>1600x1200 (UXGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="162"/>
        <source>1400x1050 (SXGA+)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="167"/>
        <source>1024x768 (XGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="172"/>
        <source>800x600 (SVGA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="187"/>
        <source>Пиксель, ширина</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="197"/>
        <source>Пиксель, высота</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="213"/>
        <source>Получение физических размеров пикселей по разрешению и размерам экрана. Используется для дальнейшей работы с угловыми минутами.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="265"/>
        <source>Размер в уголовых минутах</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="288"/>
        <source>&amp;21&apos; - Простой элемент мнемосхемы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="301"/>
        <location filename="../interfaces/part_angles.ui" line="327"/>
        <location filename="../interfaces/part_angles.ui" line="347"/>
        <source> пикс.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="314"/>
        <source>&amp;35&apos; - Составной элемент мнемосхемы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="334"/>
        <source>&amp;6&apos; - Часть составного элемента</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="360"/>
        <source>Минимально различимый глазом объект имеет размеры 0,34 угловых минуты. В качестве расчетного используем примерно 0,40. Требования к размерам мнемосхем взяты из каких-то ГОСТов от диспетчерских и пультов АСУТП. По ощущениям размер точки должен быть 1,0-1,5 для того чтобы не сильно напрягаясь читать цифры, меньше - смысла нет.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="396"/>
        <location filename="../interfaces/part_angles.ui" line="409"/>
        <source> гр.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_angles.ui" line="438"/>
        <source>Расстояние до экрана</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PartBrightness</name>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="14"/>
        <source>Brightness correction</source>
        <translation>Коррекция яркости</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="20"/>
        <source>Values</source>
        <translation>Значения</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="29"/>
        <source>Projector brightness</source>
        <translation>Световой поток от проектора</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="39"/>
        <source>Projector&apos;s aspect ratio</source>
        <translation>Отношение сторон проектора</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="49"/>
        <source>Screen&apos;s aspect ratio</source>
        <translation>Отношение сторон экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="97"/>
        <location filename="../interfaces/part_brightness.ui" line="136"/>
        <source> lm</source>
        <translation> лм</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="123"/>
        <source>Corrected brightness</source>
        <translation>Скорректированная яркость</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="152"/>
        <source>If screen&apos;s and projector&apos;s aspect ratios doesn&apos;t match than there is losses of usefull light.</source>
        <translation>Если используется не родное проектору соотношение сторон, есть непроизводительные потери светового потока. На площадь экрана приходится световой поток меньше того что отдаёт проектор.</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="165"/>
        <source>Add to notes</source>
        <translation>Добавить к тексту</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="185"/>
        <source>Area</source>
        <translation>Площадь</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="195"/>
        <source> m2</source>
        <translation> кв.м</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="214"/>
        <source>Illuminance</source>
        <translation>Освещённость</translation>
    </message>
    <message>
        <location filename="../interfaces/part_brightness.ui" line="224"/>
        <source> lm/m2</source>
        <translation> лм/м2</translation>
    </message>
</context>
<context>
    <name>PartContrast</name>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="14"/>
        <location filename="../interfaces/part_contrast.ui" line="20"/>
        <source>Контраст</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="26"/>
        <source>Паразитная засветка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="36"/>
        <location filename="../interfaces/part_contrast.ui" line="92"/>
        <location filename="../interfaces/part_contrast.ui" line="145"/>
        <source> Лм</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="49"/>
        <source>Контраст белого к выключенному</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="59"/>
        <source>:1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="79"/>
        <source>Потребность</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="109"/>
        <source>Площадь экрана</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="122"/>
        <source> кв.м</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="132"/>
        <source>Световой поток проектора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_contrast.ui" line="161"/>
        <source>Обычно берутся значения 5:1 для презентаций и 10:1 для фильмов и фотографий. Последнее требует дикую яркость или корректировку освещенности помещения.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PartDistance</name>
    <message>
        <location filename="../interfaces/part_distance.ui" line="14"/>
        <source>Distance from projector to screen</source>
        <translation>Расстояние от проектора до экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="26"/>
        <source>Distance to screen</source>
        <translation>Расстояние до экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="73"/>
        <source>Add to notes</source>
        <translation>Добавить к тексту</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="83"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="96"/>
        <location filename="../interfaces/part_distance.ui" line="125"/>
        <source> mm</source>
        <translation> мм</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="112"/>
        <source>Throw ratio</source>
        <translation>Проекционное отношение</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="141"/>
        <source>Screen width</source>
        <translation>Ширина экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/part_distance.ui" line="212"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find single value by two others.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Находит одно значение по двум другим.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>PartRoom</name>
    <message>
        <location filename="../interfaces/part_room.ui" line="14"/>
        <source>Limits by room size</source>
        <translation>Пределы по комнате</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="20"/>
        <source>Room</source>
        <translation>Комната</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="26"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="39"/>
        <location filename="../interfaces/part_room.ui" line="62"/>
        <location filename="../interfaces/part_room.ui" line="85"/>
        <location filename="../interfaces/part_room.ui" line="111"/>
        <location filename="../interfaces/part_room.ui" line="137"/>
        <location filename="../interfaces/part_room.ui" line="160"/>
        <location filename="../interfaces/part_room.ui" line="186"/>
        <location filename="../interfaces/part_room.ui" line="209"/>
        <location filename="../interfaces/part_room.ui" line="232"/>
        <location filename="../interfaces/part_room.ui" line="255"/>
        <source> mm</source>
        <translation> мм</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="52"/>
        <source>Length</source>
        <translation>Длина</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="75"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="101"/>
        <source>From floor to screen</source>
        <translation>От пола до экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="127"/>
        <source>Nearest viewer</source>
        <translation>Ближний ряд</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="150"/>
        <source>Farthest viewer</source>
        <translation>Дальний ряд</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="173"/>
        <source>Height limits (by ceiling)</source>
        <translation>Предел по высоте (физика)</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="196"/>
        <source>Minimal height (4:3)</source>
        <translation>Минимальная высота (4:3)</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="219"/>
        <source>Maximal height (4:3)</source>
        <translation>Максимальная высота (4:3)</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="242"/>
        <source>Maximal diagonal (16:9|16:10)</source>
        <translation>Максимальная диагональ (16:9|16:10)</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="271"/>
        <source>Limits of screen height because of ceiling and lower side placement. Recomendations to use doubling display if max/min values reversed.</source>
        <translation>Предел по высоте экрана (ограничение). Также рекомендации по предельным размерам в зависимости от размера комнаты и соотношения.</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="309"/>
        <source>Add to notes</source>
        <translation>Добавить к тексту</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="329"/>
        <source>Use factor</source>
        <translation>Коэфициент назначения</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="339"/>
        <location filename="../interfaces/part_room.ui" line="351"/>
        <source>important</source>
        <extracomment>6</extracomment>
        <translation>ответственный</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="346"/>
        <source>ordinary</source>
        <extracomment>8</extracomment>
        <translation>обычный</translation>
    </message>
    <message>
        <location filename="../interfaces/part_room.ui" line="356"/>
        <source>analitics</source>
        <extracomment>4</extracomment>
        <translation>повышенное внимание</translation>
    </message>
</context>
<context>
    <name>PartScreen</name>
    <message>
        <location filename="../interfaces/part_screen.ui" line="14"/>
        <source>Screen sizes</source>
        <translation>Размеры экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="20"/>
        <source>Screen</source>
        <translation>Экран</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="29"/>
        <source>Aspect ratio</source>
        <translation>Соотношение сторон</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="63"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="73"/>
        <location filename="../interfaces/part_screen.ui" line="96"/>
        <location filename="../interfaces/part_screen.ui" line="109"/>
        <source> mm</source>
        <translation> мм</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="86"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="122"/>
        <source>Diagonal</source>
        <translation>Диагональ</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="132"/>
        <source>Diagonal (inches)</source>
        <translation>Диагональ (дюймы)</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="155"/>
        <source> m2</source>
        <translation> кв.м</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="165"/>
        <source>Area</source>
        <translation>Площадь</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="188"/>
        <source>Add to notes</source>
        <translation>Добавить к тексту</translation>
    </message>
    <message>
        <location filename="../interfaces/part_screen.ui" line="201"/>
        <source>Later add here typical screen sizes from different vendors. (Like Lumien Motor Control series or projecta Electrol)</source>
        <translation>Добавить сюда типовые размеры экранов от основных производителей. (Ну или в рекомендации)</translation>
    </message>
</context>
<context>
    <name>PartSizes</name>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="14"/>
        <source>Размеры из минут</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="20"/>
        <source>Размеры</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="26"/>
        <source>Размер в уголовых минутах</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="49"/>
        <source>Расстояние до экрана</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="59"/>
        <location filename="../interfaces/part_sizes.ui" line="121"/>
        <location filename="../interfaces/part_sizes.ui" line="150"/>
        <source> мм</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="78"/>
        <source>Разрешение по горизонтали</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="105"/>
        <source>Физический размер пикселя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfaces/part_sizes.ui" line="137"/>
        <source>Ширина экрана</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectionCAD</name>
    <message>
        <location filename="../interfaces/projection.ui" line="14"/>
        <source>Projection: calculators</source>
        <translation>Проекция: калькуляторы</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="33"/>
        <source>Limits by room size</source>
        <translation>Пределы по комнате</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="46"/>
        <source>Screen sizes</source>
        <translation>Размеры экрана</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="59"/>
        <source>Aspect ratio and brightness</source>
        <translation>Формат и световой поток</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="72"/>
        <source>Throw ratio</source>
        <translation>Проекционное отношение</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="85"/>
        <source>Pixel size</source>
        <translation>Размеры пикселя</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="98"/>
        <source>Contrast</source>
        <translation>Контраст</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="111"/>
        <source>Angular size to pixel size</source>
        <translation>Угловые минуты в размеры</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="132"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="144"/>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="152"/>
        <source>&amp;Exit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="160"/>
        <source>&amp;New</source>
        <translation>&amp;Новый</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Сохранить</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="147"/>
        <source>Save notes to file.</source>
        <translation>Сохранить заметки.</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Выход</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="155"/>
        <source>Exit from calculator.</source>
        <translation>Выход из калькулятора.</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Новый</translation>
    </message>
    <message>
        <location filename="../interfaces/projection.ui" line="163"/>
        <source>Create new list of notes.</source>
        <translation>Создать новый лист заметок.</translation>
    </message>
</context>
</TS>
