<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>PartBrightnessW</name>
    <message>
        <location filename="../modules/part_brightness.py" line="84"/>
        <source>&lt;h1&gt;Brightness&lt;/h1&gt;
&lt;h2&gt;Projector&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;aspect ratio {aspect_have};&lt;/li&gt;
&lt;li&gt;brightness {brightness_before} lm.&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Screen&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;aspect ratio {aspect_want};&lt;/li&gt;
&lt;li&gt;usefull illuminance {brightness_after} lm / {area} m2 = {brightness} lx (or lm/m2).&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;br/&gt;&lt;/p&gt;
</source>
        <translation>&lt;h1&gt;Световой поток&lt;/h1&gt;
&lt;h2&gt;Проектор&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;отношением сторон {aspect_have};&lt;/li&gt;
&lt;li&gt;яркостью {brightness_before} лм.&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Сигнал&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;отношение сторон {aspect_want};&lt;/li&gt;
&lt;li&gt;полезная яркость {brightness_after} лм / {area} кв.м = {brightness} лк (или лм/м2).&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;br/&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PartDistanceW</name>
    <message>
        <location filename="../modules/part_distance.py" line="52"/>
        <source>Image on screen with width {screen} mm can be projected by projector with throw ratio {ratio} from distance {distance} mm.</source>
        <translation>Экран шириной {screen} мм засвечивается проектором с проекционным отношением {ratio} с расстояния {distance} мм.</translation>
    </message>
    <message>
        <location filename="../modules/part_distance.py" line="55"/>
        <source>Image on screen with width {screen} mm can be projected from distance {distance} mm by projector with throw ratio {ratio}.</source>
        <translation>Экран шириной {screen} мм засвечивается с расстояния {distance} мм проектором с проекционным отношением {ratio}.</translation>
    </message>
    <message>
        <location filename="../modules/part_distance.py" line="58"/>
        <source>Projector with throw ratio {ratio} from distance {distance} mm can create image on screen with width {screen} mm.</source>
        <translation>Проектор с проекционным отношением {ratio} с расстояния {distance} мм может засветить экран шириной {screen} мм.</translation>
    </message>
    <message>
        <location filename="../modules/part_distance.py" line="62"/>
        <source>&lt;h1&gt;Projection&lt;/h1&gt;&lt;p&gt;%s&lt;/p&gt;&lt;br/&gt;</source>
        <translation>&lt;h1&gt;Проекция&lt;/h1&gt;&lt;p&gt;%s&lt;/p&gt;&lt;br/&gt;</translation>
    </message>
</context>
<context>
    <name>PartRoomW</name>
    <message>
        <location filename="../modules/part_room.py" line="62"/>
        <source>&lt;em&gt;N.B.: Limits looks weird! Check the need for doubling displays/screens.s&lt;/em&gt;</source>
        <translation>&lt;em&gt;N.B.: Пределы выглядят не адекватно! Обязательно рассмотреть использование дублирующих средств отображения.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../modules/part_room.py" line="69"/>
        <source>&lt;h1&gt;Room&lt;/h1&gt;
&lt;h2&gt;Sizes&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;width: {width} mm;&lt;/li&gt;
&lt;li&gt;length: {length} mm;&lt;/li&gt;
&lt;li&gt;height: {height} mm.&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Placement&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;nearest viewer: {first} mm;&lt;/li&gt;
&lt;li&gt;farthest viewer: {last} mm;&lt;/li&gt;
&lt;li&gt;use factor multiplier: {multiplier}.&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Screen&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;height limit: {physics} mm;&lt;/li&gt;
&lt;li&gt;screen height from {minimal} to {maximal} mm;&lt;/li&gt;
&lt;li&gt;maximal diagonal: {diagonal} mm.&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;{comment}&lt;br/&gt;
&lt;/p&gt;
</source>
        <translation>&lt;h1&gt;Помещение&lt;/h1&gt;
&lt;h2&gt;Размеры&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;ширина: {width} мм;&lt;/li&gt;
&lt;li&gt;длина: {length} мм;&lt;/li&gt;
&lt;li&gt;высота: {height} мм.&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Размещение зрителей&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;ближний ряд: {first} мм;&lt;/li&gt;
&lt;li&gt;дальний ряд: {last} мм;&lt;/li&gt;
&lt;li&gt;множитель значимости системы: {multiplier}.&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Экран&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;предел по высоте: {physics} мм;&lt;/li&gt;
&lt;li&gt;высота экрана от {minimal} до {maximal} мм;&lt;/li&gt;
&lt;li&gt;максимальная диагональ: {diagonal} мм.&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;{comment}&lt;br/&gt;
&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PartScreenW</name>
    <message>
        <location filename="../modules/part_screen.py" line="99"/>
        <source>&lt;h1&gt;Screen&lt;/h1&gt;
&lt;h2&gt;Aspect ratio {aspect}&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;diagonal: {diag_inch}&quot; ({diag} mm);&lt;/li&gt;
&lt;li&gt;width: {width} mm;&lt;/li&gt;
&lt;li&gt;height: {height} mm;&lt;/li&gt;
&lt;li&gt;area: {area} square meters.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;br/&gt;&lt;/p&gt;
</source>
        <translation>&lt;h1&gt;Экран&lt;/h1&gt;
&lt;h2&gt;Соотношение сторон {aspect}&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;диагональ: {diag_inch}&quot; ({diag} мм);&lt;/li&gt;
&lt;li&gt;ширина: {width} мм;&lt;/li&gt;
&lt;li&gt;высота: {height} мм;&lt;/li&gt;
&lt;li&gt;площадь: {area} кв.м.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;br/&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ProjectionCAD</name>
    <message>
        <location filename="../modules/projection.py" line="61"/>
        <source>Question:</source>
        <translation>Вопрос:</translation>
    </message>
    <message>
        <location filename="../modules/projection.py" line="29"/>
        <source>Unsaved data exists, do you realy want to exit?</source>
        <translation type="obsolete">Есть несохраненные данные, вы действительно хотите выйти?</translation>
    </message>
    <message>
        <location filename="../modules/projection.py" line="46"/>
        <source>Save file</source>
        <translation>Сохранить в файл</translation>
    </message>
    <message>
        <location filename="../modules/projection.py" line="61"/>
        <source>Unsaved data exists, do you realy want to clear notes?</source>
        <translation type="obsolete">Есть несохраненные данные, вы дествительно хотите очистить лист заметок?</translation>
    </message>
    <message>
        <location filename="../modules/projection.py" line="61"/>
        <source>Unsaved data exists, do you really want to clear notes?</source>
        <translation>Есть несохраненные данные, вы действительно хотите очистить лист заметок?</translation>
    </message>
    <message>
        <location filename="../modules/projection.py" line="29"/>
        <source>Unsaved data exists, do you really want to exit?</source>
        <translation>Есть несохраненные данные, вы действительно хотите выйти?</translation>
    </message>
</context>
</TS>
